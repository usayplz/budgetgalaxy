package com.sayplz.budgetgalaxy.ui.expense

import android.animation.Animator
import android.animation.AnimatorSet
import android.os.Handler
import android.view.MotionEvent
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.sayplz.budgetgalaxy.R
import com.sayplz.budgetgalaxy.ui.expense.system.*
import com.sayplz.budgetgalaxy.ui.expense.system.ExpenseStates.*
import com.sayplz.budgetgalaxy.ui.system.BaseSystemViewController
import com.sayplz.budgetgalaxy.ui.system.BaseViewController
import com.sayplz.budgetgalaxy.ui.system.CircleData
import com.sayplz.budgetgalaxy.ui.system.SystemViewSizes
import com.sayplz.budgetgalaxy.ui.view.Circle
import com.sayplz.budgetgalaxy.ui.view.CircleColors
import com.sayplz.budgetgalaxy.ui.view.Core
import com.sayplz.budgetgalaxy.ui.view.Orbit
import com.sayplz.budgetgalaxy.util.addOnAnimationEnd
import com.sayplz.budgetgalaxy.util.gone
import com.sayplz.budgetgalaxy.util.hide
import com.sayplz.budgetgalaxy.util.show
import com.sayplz.data.common.doNothing
import com.sayplz.domain.entity.CalculatedDataEntity


/**
 * Created by Sergei Kurikalov
 * on 17.07.18.
 */
class ExpenseViewController(private val root: ViewGroup,
                            private val listener: ExpenseViewControllerListener, core: CircleData)
    : BaseSystemViewController() {

    private val coreViewController: CoreViewController
    private val orbitViewController: OrbitViewController
    private val asteroidViewController: AsteroidViewController
    private val satellitesViewController: SatellitesViewController
    private val countedView = root.findViewById<TextView>(R.id.counted)
    private val addButton = root.findViewById<ImageView>(R.id.add)
    private val context = root.context
    private var isExtendedState = false

    init {
        val inputStateHeight = root.height - context.resources.getDimensionPixelSize(R.dimen.grid_key_height) * 4
        val maxSize = Math.min(root.width, root.height)
        val coreSize = maxSize / 3
        val orbitSize = (maxSize / 1.5).toInt()
        val extendedOrbitSize = (maxSize * 2 / 1.5).toInt()
        val sizes = SystemViewSizes(root.width, root.height, inputStateHeight, coreSize, orbitSize, extendedOrbitSize)

        coreViewController = CoreViewController(createCore(core), isExtendedState, sizes)
        orbitViewController = OrbitViewController(createOrbit(), isExtendedState, sizes)
        asteroidViewController = AsteroidViewController(createAsteroid(), createOrbit(), sizes)
        satellitesViewController = SatellitesViewController(root, isExtendedState, sizes, listener)
    }

    fun setState(state: ExpenseStates?) {
        addButton.hide()
        when (state) {
            is Initial -> when (state.from) {
                null -> toInitialStateAnimator(state)
                is InputValue -> fromInputValueToInitialStateAnimator(state)
                is PickCategory -> fromPickCategoryToInitialStateAnimator(state)
            }
            is InputValue -> when (state.from) {
                is Initial -> fromInitialToInputStateAnimator(state)
                is PickCategory -> fromPickCategoryToInputStateAnimator(state)
            }
            is PickCategory -> when (state.from) {
                is InputValue -> fromInputValueToPickCategoryStateAnimator(state)
                is PickCategory -> fromPickCategoryToPickCategoryStateAnimator(state)
            }
            null -> doNothing()
        }
    }

    fun setCoreValue(calculatedData: CalculatedDataEntity?) {
        coreViewController.core.value = calculatedData?.displayValue ?: ""
        coreViewController.core.expression = calculatedData?.expression ?: ""
    }

    fun onTouchEvent(event: MotionEvent?) {
        satellitesViewController.gestureDetector.onTouchEvent(event)
    }

    private fun toInitialStateAnimator(state: Initial) {
        val newCore = ExpenseMapper.circleDataToCore(context, state.core)
        coreViewController.updateCore(newCore)
        setSatellites(state.satellites)
        satellitesViewController.toInitialStateAnimator()?.start()
    }

    private fun fromInputValueToInitialStateAnimator(state: Initial) {
        val newCore = ExpenseMapper.circleDataToCore(context, state.core)
        coreViewController.updateCore(newCore)
        setSatellites(state.satellites)

        playTogether(listOf(
                coreViewController.fromInputValueToInitialStateAnimator(),
                orbitViewController.fromInputValueToInitialStateAnimator(),
                satellitesViewController.fromInputValueToInitialStateAnimator(),
                asteroidViewController.fromInputValueToInitialStateAnimator()
        )).start()
    }

    private fun fromPickCategoryToInitialStateAnimator(state: Initial) {
        val newCore = ExpenseMapper.circleDataToCore(context, state.core)
        val preCoreWithoutIcon = state.preCore?.copy(icon = "") ?: return
        val preCore = ExpenseMapper.circleDataToCore(context, preCoreWithoutIcon)
        val satellite = satellitesViewController.getSatelliteById(preCore.id) ?: return
        coreViewController.updateCore(preCore, satellite.width / 2, satellite.x, satellite.y)
        setSatellites(state.satellites)

        coreViewController.core.animateText(ANIMATION_DURATION_NORMAL, 0, state.price,
                state.currency)
        countedView.show()

        playTogether(listOf(
                asteroidViewController.fromPickCategoryToInitialStateAnimator(),
                coreViewController.fromPickCategoryToInitialStateAnimator(),
                orbitViewController.fromPickCategoryToInitialStateAnimator())).apply {
            this.addOnAnimationEnd {
                Handler().postDelayed({
                    countedView.gone()
                    coreViewController.updateCore(newCore)
                    satellitesViewController.fromPickCategoryToInitialStateAnimator()?.start()
                }, 1000)
            }
            this.start()
        }
    }

    private fun fromInitialToInputStateAnimator(state: InputValue) {
        coreViewController.updateCore(ExpenseMapper.circleDataToCore(context, state.core))
        playTogether(listOf(
                coreViewController.fromInitialToInputStateAnimator(),
                orbitViewController.fromInitialToInputStateAnimator(),
                satellitesViewController.fromInitialToInputStateAnimator(),
                asteroidViewController.fromInitialToInputStateAnimator()
        )).start()
    }

    private fun fromPickCategoryToInputStateAnimator(state: InputValue) {
        coreViewController.updateCore(ExpenseMapper.circleDataToCore(context, state.core))
        return playTogether(listOf(
                coreViewController.fromPickCategoryToInputStateAnimator(),
                orbitViewController.fromPickCategoryToInputStateAnimator(),
                satellitesViewController.fromPickCategoryToInputStateAnimator(),
                asteroidViewController.fromPickCategoryToInputStateAnimator()
        )).start()
    }

    private fun fromInputValueToPickCategoryStateAnimator(state: PickCategory) {
        addButton.show()
        coreViewController.core.iconRes = null
        coreViewController.core.iconText = null
        setSatellites(state.satellites)
        asteroidViewController.text = coreViewController.core.value
        playTogether(listOf(
                coreViewController.fromInputValueToPickCategoryStateAnimator(),
                orbitViewController.fromInputValueToPickCategoryStateAnimator(),
                satellitesViewController.fromInputValueToPickCategoryStateAnimator(),
                asteroidViewController.fromInputValueToPickCategoryStateAnimator()
        )).start()
    }

    private fun fromPickCategoryToPickCategoryStateAnimator(state: PickCategory) {
        addButton.show()
        val newCore = ExpenseMapper.circleDataToCore(context, state.core)
        val satellite = satellitesViewController.getSatelliteById(newCore.id)
        when (satellite) {
            null -> coreViewController.updateCore(newCore)
            else -> coreViewController.updateCore(newCore, satellite.width / 2, satellite.x, satellite.y)
        }
        setSatellites(state.satellites)

        playTogether(listOf(
                coreViewController.fromPickCategoryToPickCategoryStateAnimator(),
                orbitViewController.fromPickCategoryToPickCategoryStateAnimator(),
                satellitesViewController.fromPickCategoryToPickCategoryStateAnimator(),
                asteroidViewController.fromPickCategoryToPickCategoryStateAnimator()
        )).start()
    }

    private fun setSatellites(satellites: List<CircleData>) {
        isExtendedState = satellites.size > BaseViewController.MIN_SATELLITES
        coreViewController.isExtendedState = isExtendedState
        orbitViewController.isExtendedState = isExtendedState
        satellitesViewController.isExtendedState = isExtendedState
        satellitesViewController.satellites = satellites.map { data ->
            ExpenseMapper.circleDataToCircle(context, data)
        }
    }

    private fun createCore(coreData: CircleData): Core {
        val core = ExpenseMapper.circleDataToCore(context, coreData)
        core.setOnClickListener { listener.onCoreClick(core.id) }
        root.addView(core)
        return core
    }

    private fun createAsteroid(): Circle {
        val colors = CircleColors.DEFAULT
                .copy(body = ContextCompat.getColor(context, R.color.core))
        return Circle(context, 0, colors, "")
                .also { root.addView(it) }
                .also { circle -> circle.setOnClickListener { listener.onAsteroidClick() } }
    }

    private fun createOrbit(): Orbit {
        return Orbit(context).also { root.addView(it) }
    }

    private fun playTogether(animators: List<Animator?>): AnimatorSet {
        listener.onLockChanged(true)
        return AnimatorSet().apply {
            val animatorList = mutableListOf<Animator>()
            animators.forEach { animator ->
                animator?.let { animatorList.add(it) }
            }
            addOnAnimationEnd { listener.onLockChanged(false) }
            playTogether(animatorList)
        }
    }

    interface ExpenseViewControllerListener {
        fun onCoreClick(id: Long)
        fun onAsteroidClick()
        fun onLockChanged(isLocked: Boolean)
        fun onSatelliteClick(id: Long)
    }
}