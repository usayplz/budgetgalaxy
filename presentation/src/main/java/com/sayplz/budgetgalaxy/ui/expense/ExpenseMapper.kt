package com.sayplz.budgetgalaxy.ui.expense

import android.content.Context
import android.graphics.Color
import com.sayplz.budgetgalaxy.ui.system.CircleData
import com.sayplz.budgetgalaxy.ui.view.Circle
import com.sayplz.budgetgalaxy.ui.view.CircleColors
import com.sayplz.budgetgalaxy.ui.view.Core
import com.sayplz.domain.entity.TreeEntity

object ExpenseMapper {
    fun circleDataToCircle(context: Context, source: CircleData): Circle {
        val iconRes = getDrawableResourceByName(context, source.icon)
        val iconText = if (iconRes == null && source.icon.isNotBlank()) source.icon else null
        val name = getStringByName(context, source.name) ?: source.name
        return Circle(
                context,
                source.id,
                source.colors,
                name,
                iconRes,
                iconText
        )
    }

    fun circleDataToCore(context: Context, source: CircleData): Core {
        val iconRes = getDrawableResourceByName(context, source.icon)
        val iconText = if (iconRes == null && source.icon.isNotBlank()) source.icon else null
        val name = getStringByName(context, source.name) ?: source.name
        return Core(
                context,
                source.id,
                source.colors,
                name,
                iconRes,
                iconText
        )
    }

    fun treeEntityToCircleData(expense: TreeEntity): CircleData {
        val name = when (expense.customTerm.isEmpty()) {
            true -> expense.term
            else -> expense.customTerm
        }

        return CircleData(
                expense.id,
                name,
                expense.icon,
                CircleColors(
                        Color.parseColor(expense.bodyColor),
                        Color.parseColor(expense.iconColor),
                        Color.parseColor(expense.titleColor),
                        Color.parseColor(expense.outerLineColor),
                        Color.parseColor(expense.innerLineColor),
                        Color.parseColor(expense.edgeColor),
                        Color.parseColor(expense.edgeColor2)
                )
        )
    }

    private fun getDrawableResourceByName(context: Context, resourceName: String): Int? {
        val identifier = context.resources.getIdentifier(resourceName, "drawable", context.packageName)
        return if (identifier == 0) null else identifier
    }

    private fun getStringByName(context: Context, resourceName: String): String? {
        val identifier = context.resources.getIdentifier(resourceName, "string", context.packageName)
        return if (identifier > 0) context.getString(identifier) else null
    }
}
