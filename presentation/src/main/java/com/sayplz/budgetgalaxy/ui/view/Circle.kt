package com.sayplz.budgetgalaxy.ui.view

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import android.util.AttributeSet
import android.view.View
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.sayplz.budgetgalaxy.R
import com.sayplz.budgetgalaxy.util.TextBitmap
import com.sayplz.budgetgalaxy.util.dimen
import java.io.Serializable


/**
 * Created by Sergei Kurikalov
 * on 22.06.18.
 */
open class Circle @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null,
                                            defStyleAttr: Int = 0) : View(context, attrs, defStyleAttr), Serializable {

    private lateinit var circlePaint: Paint
    private lateinit var ringPaint0: Paint
    private lateinit var ringPaint1: Paint
    private lateinit var ringPaint2: Paint
    private lateinit var textPaint: TextPaint

    private var animatedY = 0
    private var animatedA = 100
    protected var isAnimationPrice = false
    private var price = ""
    private lateinit var pricePaint: TextPaint
    private var currency = ""
    private lateinit var currencyPaint: TextPaint

    private val ringWidth0 = 2f
    private val ringWidth1 = 6f
    private val ringWidth2 = 2f
    private var center = PointF(0f, 0f)

    private lateinit var iconPaint: Paint
    protected var iconBitmap: Bitmap? = null
    protected val marginTiny: Int by lazy { dimen(R.dimen.space_tiny) }
    protected val marginSmall: Int by lazy { dimen(R.dimen.space_small) }
    protected val marginNormal: Int by lazy { dimen(R.dimen.space_normal) }
    protected val marginBig: Int by lazy { dimen(R.dimen.space_big) }

    open var state = State.DEFAULT
    var id = 0L
    var text = ""
        set(value) {
            field = value
            isAnimationPrice = false
            animatedY = 0
            animatedA = 100
        }

    var colors = CircleColors.DEFAULT
        set(value) {
            field = value
            createPaints()
            iconPaint = createIconPaint()
        }

    var primaryTextSize = resources.getDimension(R.dimen.font_normal)
        set(value) {
            field = value
            textPaint = createTextPaint()
        }

    var iconRes: Int? = null
        set(value) {
            field = value
            changeIcon()
        }

    var iconText: String? = null
        set(value) {
            field = value
            changeIcon()
        }

    constructor(context: Context, id: Long, colors: CircleColors, text: String,
                @DrawableRes iconRes: Int? = null, iconText: String? = null) : this(context) {
        this.id = id
        this.colors = colors
        this.text = text
        this.iconBitmap = null
        this.iconRes = iconRes
        this.iconText = iconText

        createPaints()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawRing2(canvas)
        drawRing1(canvas)
        drawRing0(canvas)
        drawCircle(canvas)
        drawText(canvas)
        iconBitmap?.let { drawIcon(canvas) }

        if (isAnimationPrice) {
            drawPrice(canvas)
            drawCurrency(canvas)
        }
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        center.x = width / 2f
        center.y = height / 2f
    }

    fun animateText(duration: Long, startDelay: Long, price: String, currency: String) {
        this.price = price
        this.currency = currency

        pricePaint = TextPaint().apply {
            isAntiAlias = true
            textSize = resources.getDimension(R.dimen.font_big)
            color = colors.title
        }

        currencyPaint = TextPaint().apply {
            isAntiAlias = true
            textSize = resources.getDimension(R.dimen.font_small)
            color = colors.title
        }

        textPaint = TextPaint().apply {
            isAntiAlias = true
            textSize = resources.getDimension(com.sayplz.budgetgalaxy.R.dimen.font_small)
            color = colors.title
        }

        isAnimationPrice = true

        ValueAnimator.ofInt(0, this.height / 5).apply {
            this.duration = duration
            this.startDelay = startDelay
            addUpdateListener { animatedY = it.animatedValue as Int }
            start()
        }

        ValueAnimator.ofInt(0, 100).apply {
            this.duration = duration
            this.startDelay = startDelay
            addUpdateListener { animatedA = it.animatedValue as Int }
            start()
        }
    }

    private fun createPaints() {
        this.circlePaint = createCirclePaint()
        this.ringPaint0 = createRingPaint0()
        this.ringPaint1 = createRingPaint1()
        this.ringPaint2 = createRingPaint2()
        this.textPaint = createTextPaint()
    }

    private fun changeIcon() {
        iconPaint = createIconPaint()
        iconBitmap = when {
            iconRes != null -> createIconBitmap()
            iconText != null -> createIconBitmapFromText()
            else -> null
        }
    }

    private fun createIconBitmap() = iconRes?.let {
        return@let getBitmapFromVectorDrawable(it)
    }

    private fun createIconBitmapFromText(): Bitmap? {
        return iconText?.let {
            val textBitmap = TextBitmap.getInstance(colors.icon,
                    context.dimen(R.dimen.font_big), context.dimen(R.dimen.icon_size))
            return@let textBitmap.create(it, 3)
        }
    }

    private fun createCirclePaint() = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        color = colors.body
    }

    protected open fun createRingPaint0() = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        color = colors.edgeColor
        strokeWidth = ringWidth0
    }

    private fun createRingPaint1() = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        color = colors.innerLineColor
        strokeWidth = ringWidth1
    }

    private fun createRingPaint2() = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        color = colors.outerLineColor
        strokeWidth = ringWidth2
    }

    private fun createTextPaint() = TextPaint().apply {
        isAntiAlias = true
        textSize = primaryTextSize
        color = colors.title
    }

    private fun createIconPaint() = Paint().apply {
        colorFilter = PorterDuffColorFilter(colors.icon, PorterDuff.Mode.SRC_IN)
    }

    protected fun drawText(canvas: Canvas) {
        if (width - 2 * marginSmall <= 0) return
        val bounds = Rect(marginNormal, marginSmall, width - marginNormal, height - marginSmall)
        val layout = StaticLayout(text, textPaint, bounds.width(),
                Layout.Alignment.ALIGN_CENTER, 1f, 0f, false)

        val x = bounds.left.toFloat()
        val y = if (state == State.TEXT_INPUT_VALUE) {
            center.y - this.height / 4 - layout.lineCount * textPaint.textSize / 2
        } else if (iconBitmap != null) {
            bounds.exactCenterY() - (bounds.exactCenterY() - bounds.top) / 2f -
                    (layout.lineCount - 1) * textPaint.textSize / 2f
        } else {
            bounds.exactCenterY() - layout.lineCount * textPaint.textSize / 2f
        }

        canvas.save()
        canvas.translate(x, y + animatedY)
        layout.draw(canvas)
        canvas.restore()
    }

    protected fun drawCircle(canvas: Canvas) {
        val radius = Math.min(width, height) / 2f - (ringWidth0 + ringWidth1 + ringWidth2)
        canvas.drawCircle(center.x, center.y, radius, circlePaint)
    }

    protected fun drawRing0(canvas: Canvas) {
        val radius = Math.min(width, height) / 2f - (ringWidth1 + ringWidth2)
        canvas.drawCircle(center.x, center.y, radius, ringPaint0)
    }

    protected fun drawRing1(canvas: Canvas) {
        val radius = Math.min(width, height) / 2f - ringWidth2
        canvas.drawCircle(center.x, center.y, radius, ringPaint1)
    }

    protected fun drawRing2(canvas: Canvas) {
        val radius = Math.min(width, height) / 2f
        canvas.drawCircle(center.x, center.y, radius, ringPaint2)
    }

    protected fun drawIcon(canvas: Canvas) {
        iconBitmap?.let { bitmap ->
            val x = center.x - bitmap.width / 2
            val y = if (state == State.TEXT_INPUT_VALUE) {
                center.y + this.height / 4
            } else {
                center.y + bitmap.height / 3
            }
            canvas.drawBitmap(bitmap, x, y, iconPaint)
        }
    }

    protected fun drawPrice(canvas: Canvas) {
//        pricePaint.alpha = animatedA
        val bounds = Rect(marginNormal, marginNormal, width - marginNormal, height - marginNormal)
        val layout = StaticLayout(price, pricePaint, bounds.width(),
                Layout.Alignment.ALIGN_CENTER, 0f, 0f, false)

        canvas.save()

        val x = bounds.left.toFloat()
        val y = bounds.exactCenterY() - pricePaint.textSize / 2
        canvas.translate(x, y)

        layout.draw(canvas)
        canvas.restore()
    }

    protected fun drawCurrency(canvas: Canvas) {
//        currencyPaint.alpha = animatedA
        val bounds = Rect(marginNormal, marginNormal, width - marginNormal, height - marginNormal)
        val layout = StaticLayout(currency, currencyPaint, bounds.width(),
                Layout.Alignment.ALIGN_CENTER, 0f, 0f, false)

        canvas.save()

        val x = bounds.left.toFloat()
        val y = bounds.exactCenterY() - pricePaint.textSize / 2 - currencyPaint.textSize - marginTiny
        canvas.translate(x, y)

        layout.draw(canvas)
        canvas.restore()
    }

    private fun getBitmapFromVectorDrawable(drawableId: Int) = try {
        val drawable = ContextCompat.getDrawable(context, drawableId)!!
        val bitmap = Bitmap.createBitmap(drawable.intrinsicWidth,
                drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        bitmap
    } catch (e: Exception) {
        null
    }

    enum class State {
        DEFAULT, TEXT_INPUT_VALUE
    }
}
