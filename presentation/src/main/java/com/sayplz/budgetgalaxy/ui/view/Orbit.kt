package com.sayplz.budgetgalaxy.ui.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.DashPathEffect
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

/**
 * Created by Sergei Kurikalov
 * on 22.06.18.
 */
class Orbit @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null,
                                      defStyleAttr: Int = 0) : View(context, attrs, defStyleAttr) {

    private val dashPath = DashPathEffect(floatArrayOf(6f, 6f, 6f, 6f), 0f)

    private val color = Color.BLACK

    private val paint = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.STROKE
        strokeWidth = 2f
        pathEffect = dashPath
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val centerX = width / 2f
        val centerY = height / 2f
        val radius = Math.min(width, height) / 2f
        paint.color = color
        canvas.drawCircle(centerX, centerY, radius, paint)
    }
}
