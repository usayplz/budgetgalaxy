package com.sayplz.budgetgalaxy.ui.keyboard

import android.animation.ObjectAnimator
import android.content.Context
import androidx.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.sayplz.budgetgalaxy.R
import com.sayplz.budgetgalaxy.ui.system.BaseSystemViewController
import com.sayplz.budgetgalaxy.util.addOnAnimationStart
import com.sayplz.budgetgalaxy.util.hide
import com.sayplz.budgetgalaxy.util.show
import com.sayplz.domain.entity.KeyboardKeyEntity

class KeyboardViewController(private val root: ViewGroup,
                             private val onClick: (KeyboardKeyEntity) -> Unit) {
    private val keyboardViews = mutableSetOf<View>()

    fun addKeyboard(keyboardType: KeyboardType) {
        removeAllKeyboardLayouts()
        when (keyboardType) {
            KeyboardType.CALCULATOR -> addView(keyboardType.layout)
        }
    }

    fun show(keyboardType: KeyboardType) {
        val view = keyboardViews.firstOrNull { it.tag == keyboardType.layout } ?: return
        ObjectAnimator.ofFloat(view, View.ALPHA, 0f, 1f).apply {
            duration = BaseSystemViewController.ANIMATION_DURATION_MIN
            startDelay = BaseSystemViewController.ANIMATION_DURATION_MIN
            addOnAnimationStart {
                view.alpha = 0f
                view.show()
            }
            start()
        }
    }

    fun hide() {
        keyboardViews.forEach { it.hide() }
    }

    private fun addView(@LayoutRes layout: Int) {
        val view: ViewGroup = createKeyboard(layout)
        root.addView(view)
        keyboardViews.add(view)
        setOnClickListeners(view)
    }

    private fun setOnClickListeners(view: ViewGroup) {
        for (i in 0..view.childCount) {
            val v = view.getChildAt(i) ?: continue
            v.setOnClickListener { onClick(v.tag.toString().toKey()) }
            v.setOnLongClickListener { setOnLongClick(v) }
        }
    }

    private fun setOnLongClick(v: View): Boolean {
        if (v.tag.toKey() == KeyboardKeyEntity.BACKSPACE) {
            onClick(KeyboardKeyEntity.LONG_BACKSPACE)
        }
        return true
    }

    private fun createKeyboard(layout: Int): ViewGroup {
        val inflater = root.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                as LayoutInflater
        return (inflater.inflate(layout, null) as ViewGroup).apply {
            this.tag = layout
            this.hide()
            fixLanguageComma(this)
        }
    }

    private fun fixLanguageComma(view: ViewGroup) {
        view.findViewById<Button>(R.id.comma).apply {
            text = KeyboardKeyEntity.COMMA.tag
            tag = KeyboardKeyEntity.COMMA.tag
        }
    }

    private fun Any.toKey(): KeyboardKeyEntity {
        KeyboardKeyEntity.values().forEach {
            if (it.tag == this.toString()) return it
        }
        return KeyboardKeyEntity.OK
    }

    private fun removeAllKeyboardLayouts() {
        keyboardViews.forEach { root.removeView(it) }
        keyboardViews.clear()
    }

    enum class KeyboardType(@LayoutRes val layout: Int) {
        CALCULATOR(R.layout.view_keyboard_calculator)
    }
}
