package com.sayplz.budgetgalaxy.ui.expense.system

import android.animation.Animator
import android.animation.ObjectAnimator
import android.view.View
import com.sayplz.budgetgalaxy.R
import com.sayplz.budgetgalaxy.ui.system.BaseSystemViewController
import com.sayplz.budgetgalaxy.ui.system.BaseViewController
import com.sayplz.budgetgalaxy.ui.system.SystemViewSizes
import com.sayplz.budgetgalaxy.ui.view.Orbit

/**
 * Created by Sergei Kurikalov
 * on 17.07.18.
 */
class OrbitViewController(private val orbit: Orbit, var isExtendedState: Boolean, sizes: SystemViewSizes)
    : BaseViewController(sizes.width, sizes.height), ExpenseStatesInterface {

    private val duration: Long
    private val durationMin: Long
    private val marginTiny: Int

    private val size: Int
    private val x: Float
    private val y: Float

    private val inputStateX: Float
    private val inputStateY: Float

    private val extendedSize: Int
    private val extendedStateX: Float
    private val extendedStateY: Float

    init {
        duration = BaseSystemViewController.ANIMATION_DURATION_NORMAL
        durationMin = BaseSystemViewController.ANIMATION_DURATION_MIN
        marginTiny = orbit.context.resources.getDimensionPixelSize(R.dimen.space_tiny)

        size = sizes.orbitSize
        x = center.x - size / 2f
        y = center.y - size / 2f

        inputStateX = x
        inputStateY = Math.min(sizes.width, sizes.inputStateHeight) / 2f - size / 2f

        extendedSize = sizes.extendedOrbitSize
        extendedStateX = sizes.width - 2f * marginTiny - extendedSize / 2 - sizes.coreSize / 2
        extendedStateY = sizes.height - 2f * marginTiny - extendedSize / 2 - sizes.coreSize / 2

        orbit.updateParams(size, x, y)
    }

    override fun toInitialStateAnimator(): Animator? {
        return orbit.createMorphAnimator(size, getToX(), getToY(), duration)
    }

    override fun fromInitialToInputStateAnimator(): Animator? {
        return orbit.createMorphAnimator(size, inputStateX, inputStateY, duration)
    }

    override fun fromInputValueToPickCategoryStateAnimator(): Animator? {
        orbit.alpha = 0f
        orbit.updateParams(getToSize(), getToX(), getToY())
        return ObjectAnimator.ofFloat(orbit, View.ALPHA, 1f).apply {
            duration = durationMin
            startDelay = durationMin
        }
    }

    override fun fromInputValueToInitialStateAnimator(): Animator? {
        return fromInputValueToPickCategoryStateAnimator()
    }

    override fun fromPickCategoryToInputStateAnimator(): Animator? {
        return fromInitialToInputStateAnimator()
    }

    override fun fromPickCategoryToInitialStateAnimator(): Animator? {
        return fromInputValueToPickCategoryStateAnimator()
    }

    override fun fromPickCategoryToPickCategoryStateAnimator(): Animator? {
        return fromInputValueToPickCategoryStateAnimator()
    }

    private fun getToSize() = if (isExtendedState) extendedSize else size

    private fun getToX() = if (isExtendedState) extendedStateX else x

    private fun getToY() = if (isExtendedState) extendedStateY else y
}
