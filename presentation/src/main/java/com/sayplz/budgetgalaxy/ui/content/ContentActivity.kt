package com.sayplz.budgetgalaxy.ui.content

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.sayplz.budgetgalaxy.R
import com.sayplz.budgetgalaxy.ui.expense.ExpenseFragment


class ContentActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content)
        setFragment()
    }

    private fun setFragment() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, ExpenseFragment())
                .commit()
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.container)
        if (fragment !is IOnBackPressed || !(fragment as IOnBackPressed).onBackPressed()) {
            super.onBackPressed()
        }
    }

    interface IOnBackPressed {
        fun onBackPressed(): Boolean
    }
}