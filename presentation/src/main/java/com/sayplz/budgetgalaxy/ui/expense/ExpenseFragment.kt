package com.sayplz.budgetgalaxy.ui.expense

import androidx.lifecycle.Observer
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import com.sayplz.budgetgalaxy.R
import com.sayplz.budgetgalaxy.databinding.FragmentExpenseBinding
import com.sayplz.budgetgalaxy.ui.content.ContentActivity
import com.sayplz.budgetgalaxy.ui.expense.system.ExpenseStates
import com.sayplz.budgetgalaxy.ui.keyboard.KeyboardViewController
import org.koin.androidx.viewmodel.ext.android.viewModel

class ExpenseFragment : Fragment(), ContentActivity.IOnBackPressed {
    private lateinit var binding: FragmentExpenseBinding
    private lateinit var viewsController: ExpenseViewController
    private lateinit var keyboard: KeyboardViewController
    private val viewModel: ExpenseViewModel by viewModel()
    private lateinit var globalLayoutListener: ViewTreeObserver.OnGlobalLayoutListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_expense, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    override fun onBackPressed(): Boolean {
        return viewModel.back()
    }

    private fun setup() {
        globalLayoutListener = ViewTreeObserver.OnGlobalLayoutListener {
            binding.system.viewTreeObserver.removeOnGlobalLayoutListener(globalLayoutListener)
            setupViewModel()
            setupKeyboardView()
            setupBindings()
        }

        binding.system.viewTreeObserver.addOnGlobalLayoutListener(globalLayoutListener)
    }

    private fun setupViewModel() {
        viewModel.state.observe(this, Observer { renderState(it) })
        viewModel.inputValue.observe(this, Observer {
            if (::viewsController.isInitialized) viewsController.setCoreValue(it)
        })
    }

    private fun setupKeyboardView() {
        keyboard = KeyboardViewController(binding.keyboardContainter) { viewModel.onKeyboardClick(it) }
        keyboard.addKeyboard(KeyboardViewController.KeyboardType.CALCULATOR)
    }

    private fun setupGestures() {
        view?.setOnTouchListener { _, event ->
            viewsController.onTouchEvent(event)
            return@setOnTouchListener true
        }
    }

    private fun renderState(state: ExpenseStates?) {
        setupViewsController(state)
        setKeyboardVisibility(state)
        viewsController.setState(state)
    }

    private fun setupViewsController(state: ExpenseStates?) {
        if (state is ExpenseStates.Initial && !::viewsController.isInitialized) {
            viewsController = ExpenseViewController(binding.system, viewModel, state.core)
            setupGestures()
        }
    }

    private fun setKeyboardVisibility(state: ExpenseStates?) {
        if (state is ExpenseStates.InputValue) {
            keyboard.show(KeyboardViewController.KeyboardType.CALCULATOR)
        } else {
            keyboard.hide()
        }
    }

    private fun setupBindings() {
        binding.add.setOnClickListener {  }
    }
}
