package com.sayplz.budgetgalaxy.ui.expense.system

import android.animation.Animator

/**
 * Created by Sergei Kurikalov
 * on 25.07.18.
 */
interface ExpenseStatesInterface {
    fun toInitialStateAnimator(): Animator?
    fun fromInitialToInputStateAnimator(): Animator?
    fun fromInputValueToPickCategoryStateAnimator(): Animator?
    fun fromInputValueToInitialStateAnimator(): Animator?
    fun fromPickCategoryToInputStateAnimator(): Animator?
    fun fromPickCategoryToInitialStateAnimator(): Animator?
    fun fromPickCategoryToPickCategoryStateAnimator(): Animator?
}