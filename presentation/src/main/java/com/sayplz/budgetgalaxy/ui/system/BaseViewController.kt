package com.sayplz.budgetgalaxy.ui.system

import android.animation.Animator
import android.animation.ValueAnimator
import android.graphics.PointF
import android.view.View

abstract class BaseViewController(private val width: Int, height: Int) {
    companion object {
        const val MIN_SATELLITES = 6
        const val MAX_SATELLITES = 8
    }

    val center = PointF(width / 2f, height / 2f)
    val maxSize = Math.min(width, height)

    fun View.createMorphAnimator(newSize: Int, newX: Float, newY: Float, duration: Long): Animator {
        val oldX = this.x
        val oldY = this.y
        return ValueAnimator.ofInt(this.width, newSize).apply {
            this.duration = duration
            this.addUpdateListener {
                this@createMorphAnimator.updateAnimation(animatedValue as Int, oldX, oldY, newX,
                        newY, animatedFraction)
            }
        }
    }

    fun View.updateAnimation(newSize: Int, oldX: Float, oldY: Float, newX: Float, newY: Float,
                             fraction: Float) {
        val x = oldX - (oldX - newX) * fraction
        val y = oldY - (oldY - newY) * fraction
        this.updateParams(newSize, x, y)
    }

    fun View.updateParams(newSize: Int, newX: Float, newY: Float) {
        this.layoutParams.width = newSize
        this.layoutParams.height = newSize
        this.x = newX
        this.y = newY
        this.requestLayout()
    }
}
