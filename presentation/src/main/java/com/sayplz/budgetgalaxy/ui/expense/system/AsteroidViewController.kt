package com.sayplz.budgetgalaxy.ui.expense.system

import android.animation.Animator
import android.animation.AnimatorSet
import com.sayplz.budgetgalaxy.R
import com.sayplz.budgetgalaxy.ui.system.BaseSystemViewController
import com.sayplz.budgetgalaxy.ui.system.BaseViewController
import com.sayplz.budgetgalaxy.ui.system.SystemViewSizes
import com.sayplz.budgetgalaxy.ui.view.Circle
import com.sayplz.budgetgalaxy.ui.view.Orbit
import com.sayplz.budgetgalaxy.util.addOnAnimationEnd
import com.sayplz.budgetgalaxy.util.hide
import com.sayplz.budgetgalaxy.util.show

/**
 * Created by Sergei Kurikalov
 * on 17.07.18.
 */
class AsteroidViewController(private val asteroid: Circle, private val orbit: Orbit,
                             private val sizes: SystemViewSizes)
    : BaseViewController(sizes.width, sizes.height), ExpenseStatesInterface {

    var text = ""
        set(text) {
            field = text
            asteroid.text = text
        }

    private val duration = BaseSystemViewController.ANIMATION_DURATION_NORMAL
    private val marginTiny = asteroid.context.resources.getDimensionPixelSize(R.dimen.space_tiny)

    private val size = sizes.orbitSize - marginTiny
    private val x = center.x - size / 2f
    private val y = Math.min(sizes.width, sizes.inputStateHeight) / 2f - size / 2f
    private val pickStateSize = (sizes.orbitSize * 0.9 - marginTiny).toInt()
    private val pickStateX = -size / 5f
    private val pickStateY = -size / 3f

    private val orbitSize = sizes.orbitSize
    private val orbitX = center.x - orbitSize / 2f
    private val orbitY = Math.min(sizes.width, sizes.inputStateHeight) / 2f - orbitSize / 2f
    private val orbitPickSize = (sizes.orbitSize * 0.9).toInt()
    private val orbitPickStateX = -orbitSize / 5f
    private val orbitPickStateY = -orbitSize / 3f

    init {
        asteroid.updateParams(size, x, y)
        asteroid.hide()

        orbit.updateParams(orbitSize, orbitX, orbitY)
        orbit.hide()
    }

    override fun toInitialStateAnimator(): Animator? {
        asteroid.hide()
        orbit.hide()
        return null
    }

    override fun fromInitialToInputStateAnimator(): Animator? {
        return toInitialStateAnimator()
    }

    override fun fromInputValueToPickCategoryStateAnimator(): Animator? {
        asteroid.updateParams(size, x, y)
        asteroid.show()
        asteroid.bringToFront()

        val asteroidMorphAnimator = asteroid.createMorphAnimator(pickStateSize, pickStateX,
                pickStateY, duration)

        orbit.updateParams(orbitSize, orbitX, orbitY)
        orbit.show()
        val orbitMorphAnimator = orbit.createMorphAnimator(orbitPickSize, orbitPickStateX,
                orbitPickStateY, duration)

        return AnimatorSet().also { it.playTogether(asteroidMorphAnimator, orbitMorphAnimator) }
    }

    override fun fromInputValueToInitialStateAnimator(): Animator? {
        return toInitialStateAnimator()
    }

    override fun fromPickCategoryToInputStateAnimator(): Animator? {
        asteroid.updateParams(pickStateSize, pickStateX, pickStateY)
        asteroid.show()
        val asteroidMorphAnimator = asteroid.createMorphAnimator(size, x, y, duration)
                .also { it.addOnAnimationEnd { asteroid.hide() } }

        orbit.updateParams(orbitPickSize, orbitPickStateX, orbitPickStateY)
        orbit.show()
        val orbitMorphAnimator = orbit.createMorphAnimator(orbitSize, orbitX,
                orbitY, duration).also { it.addOnAnimationEnd { orbit.hide() } }

        return AnimatorSet().also { it.playTogether(asteroidMorphAnimator, orbitMorphAnimator) }
    }

    override fun fromPickCategoryToInitialStateAnimator(): Animator? {
        asteroid.updateParams(pickStateSize, pickStateX, pickStateY)
        asteroid.show()
        val toX = center.x - sizes.coreSize / 2f
        val toY = center.y - sizes.coreSize / 2f
        val asteroidAnimator = asteroid.createMorphAnimator(sizes.coreSize, toX, toY, duration)
                .also {
                    it.addOnAnimationEnd {
                        asteroid.updateParams(size, x, y)
                        asteroid.hide()
                    }
                }

        orbit.updateParams(orbitPickSize, orbitPickStateX, orbitPickStateY)
        orbit.show()
        val toOrbitY = center.y - orbitSize / 2f
        val orbitAnimator = orbit.createMorphAnimator(orbitSize, orbitX,
                toOrbitY, duration)

        return AnimatorSet().also { it.playTogether(asteroidAnimator, orbitAnimator) }
    }

    override fun fromPickCategoryToPickCategoryStateAnimator(): Animator? {
        return null
    }
}
