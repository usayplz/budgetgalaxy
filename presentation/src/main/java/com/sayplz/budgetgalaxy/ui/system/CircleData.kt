package com.sayplz.budgetgalaxy.ui.system

import com.sayplz.budgetgalaxy.ui.view.CircleColors

data class CircleData(
        val id: Long,
        val name: String,
        val icon: String,
        val colors: CircleColors
)
