package com.sayplz.budgetgalaxy.ui.view

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Animation
import androidx.annotation.DrawableRes
import com.sayplz.budgetgalaxy.R


/**
 * Created by Sergei Kurikalov
 * on 25.07.18.
 */
class Core(context: Context, id: Long, colors: CircleColors, text: String,
           @DrawableRes iconRes: Int? = null, iconText: String? = null)
    : Circle(context, id, colors, text, iconRes, iconText) {

    private val valuePaint: TextPaint
    private val expressionPaint: TextPaint
    private val valueBackgroundPaint: Paint
    private val cursorPaint: Paint
    private var cursorAlpha = 100
    private val cursorAnimator: ValueAnimator

    var value = ""
        set(value) {
            field = value
            invalidate()
        }

    var expression = ""
        set(value) {
            field = value
            invalidate()
        }

    override var state = State.DEFAULT
        set(value) {
            field = value
            if (value == State.TEXT_INPUT_VALUE) {
                cursorAnimator.start()
            } else {
                cursorAnimator.cancel()
            }
        }

    init {
        this.valuePaint = createValuePaint()
        this.expressionPaint = createExpressionPaint()
        this.valueBackgroundPaint = createValueBackgroundPaint()
        this.cursorPaint = createCursorPaint()
        this.cursorAnimator = createCursorAnimator()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawRing2(canvas)
        drawRing1(canvas)
        drawRing0(canvas)
        drawCircle(canvas)
        drawText(canvas)

        iconBitmap?.let { drawIcon(canvas) }
        if (state == State.TEXT_INPUT_VALUE) {
            drawValueBackground(canvas)
            drawValueText(canvas)
            drawExpressionText(canvas)
            drawCursor(canvas)
        }

        if (isAnimationPrice) {
            drawPrice(canvas)
            drawCurrency(canvas)
        }
    }

    private fun createCursorAnimator() = ValueAnimator.ofInt(1, 100).apply {
        duration = 700
        interpolator = AccelerateDecelerateInterpolator()
        repeatCount = Animation.INFINITE
        repeatMode = ValueAnimator.REVERSE
        addUpdateListener {
            cursorAlpha = animatedValue as Int
            invalidate()
        }
    }

    private fun createValuePaint() = TextPaint().apply {
        isAntiAlias = true
        textSize = resources.getDimension(R.dimen.font_big)
        color = Color.DKGRAY
    }

    private fun createCursorPaint() = Paint().apply {
        isAntiAlias = true
        color = Color.DKGRAY
        strokeWidth = 3f
    }

    private fun createExpressionPaint() = TextPaint().apply {
        isAntiAlias = true
        textSize = resources.getDimension(R.dimen.font_tiny)
        color = Color.LTGRAY
    }

    private fun createValueBackgroundPaint() = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        color = Color.WHITE
    }

    override fun createRingPaint0() = super.createRingPaint0().apply {
        color = colors.edgeColor2
    }

    private fun drawValueText(canvas: Canvas) {
        val bounds = Rect(marginNormal, marginNormal, width - marginNormal, height - marginNormal)
        val layout = StaticLayout(value, valuePaint, bounds.width(),
                Layout.Alignment.ALIGN_CENTER, 0f, 0f, false)

        canvas.save()

        val x = bounds.left.toFloat()
        val y = bounds.exactCenterY() - valuePaint.textSize / 2
        canvas.translate(x, y)

        layout.draw(canvas)
        canvas.restore()
    }

    private fun drawCursor(canvas: Canvas) {
        val bounds = Rect(marginNormal, marginNormal, width - marginNormal, height - marginNormal)
        if (expression.isEmpty()) {
            cursorPaint.color = Color.DKGRAY
            cursorPaint.alpha = cursorAlpha
            val startX = bounds.exactCenterX() + valuePaint.measureText(value) / 2 + marginTiny
            val startY = bounds.exactCenterY() - valuePaint.textSize / 2 + 2
            val stopY = startY + valuePaint.textSize
            canvas.drawLine(startX, startY, startX, stopY, cursorPaint)
        } else {
            cursorPaint.color = Color.LTGRAY
            cursorPaint.alpha = cursorAlpha
            val valueBackgroundBottom = bounds.exactCenterY() + valuePaint.textSize + marginSmall
            val startX = bounds.exactCenterX() + expressionPaint.measureText(expression) / 2 + marginTiny
            val startY = valueBackgroundBottom + marginSmall + 2
            val stopY = startY + expressionPaint.textSize
            canvas.drawLine(startX, startY, startX, stopY, cursorPaint)
        }
    }

    private fun drawValueBackground(canvas: Canvas) {
        val top = height / 2 - valuePaint.textSize - marginTiny
        val bottom = height / 2 + valuePaint.textSize + marginTiny
        val rect = RectF(marginBig.toFloat(), top, width - marginBig.toFloat(), bottom)
        canvas.drawRoundRect(rect, 12f, 12f, valueBackgroundPaint)
    }

    private fun drawExpressionText(canvas: Canvas) {
        val bounds = Rect(marginNormal, marginNormal, width - marginNormal, height - marginNormal)
        val layout = StaticLayout(expression, expressionPaint, bounds.width(),
                Layout.Alignment.ALIGN_CENTER, 0f, 0f, false)

        canvas.save()

        val valueBackgroundBottom = bounds.exactCenterY() + valuePaint.textSize + marginSmall
        val x = bounds.left.toFloat()
        val y = valueBackgroundBottom + marginSmall
        canvas.translate(x, y)

        layout.draw(canvas)
        canvas.restore()
    }
}
