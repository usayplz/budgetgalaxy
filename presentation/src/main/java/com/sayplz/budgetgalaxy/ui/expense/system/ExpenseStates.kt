package com.sayplz.budgetgalaxy.ui.expense.system

import com.sayplz.budgetgalaxy.ui.system.CircleData

/**
 * Created by Sergei Kurikalov
 * on 23.07.18.
 */
sealed class ExpenseStates() {
    data class Initial(val from: ExpenseStates?, val satellites: List<CircleData>,
                       val core: CircleData, val preCore: CircleData?, val price: String,
                       val currency: String) : ExpenseStates()

    data class InputValue(val from: ExpenseStates?, val core: CircleData) : ExpenseStates()

    data class PickCategory(val from: ExpenseStates?, val satellites: List<CircleData>,
                            val core: CircleData) : ExpenseStates()
}
