package com.sayplz.budgetgalaxy.ui.expense.system

import android.animation.Animator
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.graphics.Path
import android.graphics.PointF
import android.graphics.Rect
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import com.sayplz.budgetgalaxy.R
import com.sayplz.budgetgalaxy.ui.expense.ExpenseViewController
import com.sayplz.budgetgalaxy.ui.system.BaseSystemViewController
import com.sayplz.budgetgalaxy.ui.system.BaseViewController
import com.sayplz.budgetgalaxy.ui.system.SystemViewSizes
import com.sayplz.budgetgalaxy.ui.view.Circle
import com.sayplz.budgetgalaxy.util.gone
import com.sayplz.budgetgalaxy.util.hide
import com.sayplz.budgetgalaxy.util.show
import kotlin.math.sign
import kotlin.math.sqrt

class SatellitesViewController(private val root: ViewGroup, var isExtendedState: Boolean,
                               private val sizes: SystemViewSizes,
                               private val listener: ExpenseViewController.ExpenseViewControllerListener)
    : BaseViewController(sizes.width, sizes.height), ExpenseStatesInterface {

    private val marginTiny: Int
    private val textSize: Float

    private var radius: Int
    private var pointCenter: PointF

    private val size: Int
    private val animators = mutableListOf<ObjectAnimator>()

    val gestureDetector: GestureDetector by lazy {
        GestureDetector(root.context, GestureDetectorListener())
    }

    var satellites = listOf<Circle>()
        set(value) {
            field.forEach { root.removeView(it) }
            value.forEach { s -> root.addView(s).also { s.hide() } }
            field = value
            calcPosition()
            setSatellitesPosition()
        }

    init {
        marginTiny = root.context.resources.getDimensionPixelSize(R.dimen.space_tiny)
        textSize = root.context.resources.getDimension(R.dimen.font_small)
        size = maxSize / 3 - maxSize / 15
        radius = sizes.orbitSize / 2
        pointCenter = PointF(center.x, center.y)

        setSatellitesPosition()
    }


    fun getSatelliteById(id: Long): Circle? {
        return satellites.firstOrNull { it.id == id }
    }

    override fun toInitialStateAnimator(): Animator? {
        satellites.forEach {
            it.alpha = 0f
            it.show()
        }

        return ValueAnimator.ofFloat(0f, 1f).apply {
            duration = BaseSystemViewController.ANIMATION_DURATION_NORMAL
            startDelay = BaseSystemViewController.ANIMATION_DURATION_NORMAL
            addUpdateListener { _ ->
                satellites.forEach { it.alpha = animatedValue as Float }
            }
        }
    }

    override fun fromInitialToInputStateAnimator(): Animator? {
        satellites.forEach { it.gone() }
        return null
    }

    override fun fromInputValueToPickCategoryStateAnimator(): Animator? {
        return toInitialStateAnimator()
    }

    override fun fromInputValueToInitialStateAnimator(): Animator? {
        return toInitialStateAnimator()
    }

    override fun fromPickCategoryToInputStateAnimator(): Animator? {
        satellites.forEach { it.gone() }
        return null
    }

    override fun fromPickCategoryToInitialStateAnimator(): Animator? {
        return toInitialStateAnimator()
    }

    override fun fromPickCategoryToPickCategoryStateAnimator(): Animator? {
        return toInitialStateAnimator()
    }

    private fun calcPosition() {
        radius = if (isExtendedState) sizes.extendedOrbitSize / 2 else sizes.orbitSize / 2
        pointCenter = if (isExtendedState) {
            val x = sizes.width - sizes.coreSize / 2f - marginTiny
            val y = sizes.height - sizes.coreSize / 2f - marginTiny
            PointF(x, y)

        } else {
            PointF(center.x, center.y)
        }
    }

    private fun setSatellitesPosition() {
        if (satellites.isEmpty()) return

        val minAngle = 360.0 / satellites.size
        satellites.forEachIndexed { i, circle ->
            circle.primaryTextSize = textSize
            val x = pointCenter.x - size / 2 + (radius * Math.cos(Math.toRadians(minAngle * i))).toFloat()
            val y = pointCenter.y - size / 2 + (radius * Math.sin(Math.toRadians(minAngle * i))).toFloat()
            circle.updateParams(size, x, y)
        }
    }

    private fun getAngle(circle: Circle): Double {
        val deltaX = (circle.x + circle.width / 2) - pointCenter.x
        val deltaY = (circle.y + circle.width / 2) - pointCenter.y
        val rad = Math.atan2(deltaY.toDouble(), deltaX.toDouble())
        return rad * (180 / Math.PI)
    }

    private fun moveSatellitesWithDelta(delta: Int) {
        animators.forEach { it.cancel() }
        animators.clear()

        satellites.forEach { circle ->
            val path = createPath(circle, delta)
            val animator = ObjectAnimator.ofFloat(circle, View.X, View.Y, path)
            animator.duration = 0
            animator.start()
        }
    }

    private fun rotateSatellitesWithDelta(delta: Int) {
        animators.forEach { it.cancel() }
        animators.clear()

        satellites.forEach { circle ->
            val path = createPath(circle, delta)
            val animator = ObjectAnimator.ofFloat(circle, View.X, View.Y, path)
            animator.duration = (2000 * Math.abs(delta) / 360).toLong()
            animator.interpolator = DecelerateInterpolator()
            animator.start()
            animators.add(animator)
        }
    }

    private fun createPath(circle: Circle, distance: Int): Path {
        val path = Path()
        val angle = getAngle(circle)

        path.moveTo(circle.x, circle.y)
        var i = 0
        while (i != distance) {
            path.lineTo(pointCenter.x - size / 2 + (radius * Math.cos(Math.toRadians(angle + i))).toFloat(),
                    pointCenter.y - size / 2 + (radius * Math.sin(Math.toRadians(angle + i))).toFloat())
            if (distance > 0) i++ else i--
        }
        return path
    }

    private fun isViewInBounds(view: View, x: Float, y: Float): Boolean {
        val outRect = Rect()
        val location = IntArray(2)
        view.getDrawingRect(outRect)
        view.getLocationOnScreen(location)
        outRect.offset(location[0], location[1])
        return outRect.contains(x.toInt(), y.toInt())
    }

    inner class GestureDetectorListener : GestureDetector.SimpleOnGestureListener() {
        override fun onSingleTapUp(e: MotionEvent?): Boolean {
            if (e == null) return super.onSingleTapUp(e)
            satellites.forEach {
                if (isViewInBounds(it, e.x, e.y)) {
                    listener.onSatelliteClick(it.id)
                    return@forEach
                }
            }
            return super.onSingleTapUp(e)
        }


        override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {
            if (e1 == null || e2 == null) return false

            val isBottom = (e2.y > pointCenter.y)
            val isRight = (e2.x > pointCenter.x)

            val deltaX = isBottom.toSign() * distanceX
            val deltaY = isRight.not().toSign() * distanceY
            val delta = calcTotalDelta(deltaX, deltaY) / 3
            moveSatellitesWithDelta(delta)

            return true
        }

        override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {
            if (e1 == null || e2 == null) return false

            val isBottom = (e2.y > pointCenter.y)
            val isRight = (e2.x > pointCenter.x)

            val deltaX = isBottom.toSign() * -velocityX
            val deltaY = isRight.not().toSign() * -velocityY
            val delta = calcTotalDelta(deltaX, deltaY) / 10
            rotateSatellitesWithDelta(delta)

            return true
        }

        private fun calcTotalDelta(deltaX: Float, deltaY: Float): Int {
            val sign = if (Math.abs(deltaX) > Math.abs(deltaY)) sign(deltaX) else sign(deltaY)
            return (sqrt(deltaX * deltaX + deltaY * deltaY) * sign).toInt()
        }

        private fun Boolean.toSign() = if (this) 1 else -1
    }
}
