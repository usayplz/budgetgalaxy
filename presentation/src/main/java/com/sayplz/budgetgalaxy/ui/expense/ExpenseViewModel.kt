package com.sayplz.budgetgalaxy.ui.expense

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.sayplz.budgetgalaxy.common.BaseViewModel
import com.sayplz.budgetgalaxy.ui.expense.system.ExpenseStates
import com.sayplz.budgetgalaxy.ui.expense.system.ExpenseStates.*
import com.sayplz.budgetgalaxy.ui.system.CircleData
import com.sayplz.budgetgalaxy.util.info
import com.sayplz.data.common.Constants.EXPENSE_CORE_ID
import com.sayplz.domain.base.Schedulers
import com.sayplz.domain.entity.*
import com.sayplz.domain.entity.KeyboardKeyEntity.OK
import com.sayplz.domain.usecase.*
import io.reactivex.Observable
import io.reactivex.functions.Function3
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class ExpenseViewModel(private val addKey: AddKeyUseCase,
                       private val resetCalculatedData: ResetCalculatedDataUseCase,
                       private val getExpensesByParentId: GetExpensesByParentIdUseCase,
                       getConfig: GetConfigUseCase, schedules: Schedulers,
                       getExpenseCore: GetExpenseCoreUseCase) : BaseViewModel(),
        ExpenseViewController.ExpenseViewControllerListener {

    private val config = mutableListOf<ConfigEntity>()
    private val stateStack = MutableLiveData<MutableList<ExpenseStates>>()
    private val pickCategorySatellites: MutableList<CircleData> = mutableListOf()
    private lateinit var core: CircleData
    private var isLocked = false

    val state = Transformations.map(stateStack) { it.last() }
    val inputValue = MutableLiveData<CalculatedDataEntity>()

    init {
        getExpenseCore.execute().toObservable()
                .doOnNext { this.core = ExpenseMapper.treeEntityToCircleData(it) }
                .observeOn(schedules.io)
                .switchMap {
                    Observable.zip(
                            getExpensesByParentId.execute(it.id).toObservable(),
                            getConfig.execute().toObservable(),
                            getExpensesByParentId.execute(0).toObservable(),
                            Function3<List<TreeEntity>, List<ConfigEntity>, List<TreeEntity>,
                                    Triple<List<TreeEntity>, List<ConfigEntity>, List<TreeEntity>>>
                            { expenses, config, actions ->
                                return@Function3 Triple(expenses, config, actions)
                            })
                }
                .observeOn(schedules.ui)
                .subscribeBy { triple ->
                    val circleDataList = triple.first.map { ExpenseMapper.treeEntityToCircleData(it) }
                    pickCategorySatellites.addAll(circleDataList)

                    config.clear()
                    config.addAll(triple.second)

                    setInitialState(resetCalculatedData.execute(), triple.third)
                }
    }

    override fun onLockChanged(isLocked: Boolean) {
        this.isLocked = isLocked
    }

    override fun onCoreClick(id: Long) {
        if (id == core.id && state.value is Initial) {
            getNextState()?.let { pushState(it) }
        }
    }

    override fun onAsteroidClick() {
        stateStack.value?.forEachIndexed { i, state ->
            if (state is InputValue) {
                for (j in i + 1 until stateStack.value!!.size - 1) stateStack.value?.removeAt(j)
                popState()
                return
            }
        }
    }

    override fun onSatelliteClick(id: Long) {
        val state = state.value
        when (state) {
            is ExpenseStates.Initial -> state.satellites.firstOrNull { it.id == id }
            is ExpenseStates.PickCategory -> getSubcategories(id, state)
        }
    }

    private fun getSubcategories(id: Long, currentState: PickCategory) {
        getExpensesByParentId.execute(id)
                .doOnError { info(it) }
                .subscribe { expenses ->
                    if (expenses.isEmpty()) {
                        val initial = getSavedInitialState(currentState)
                        val satellite = currentState.satellites.firstOrNull { it.id == id }
                        pushState(initial.copy(preCore = satellite))
                    } else {
                        val satellites = expenses.map {
                            ExpenseMapper.treeEntityToCircleData(it)
                        }
                        val newCore = pickCategorySatellites.first { it.id == id }
                        val newState = PickCategory(currentState, satellites, newCore)
                        pushState(newState)
                    }
                }
                .addTo(subscription)
    }

    fun onKeyboardClick(key: KeyboardKeyEntity) {
        if (key != OK) {
            inputValue.value = addKey.execute(key)
        } else if (inputValue.value?.displayValue?.isNotEmpty() == true) {
            getNextState()?.let { pushState(it) }
        }
    }

    fun back() = when (state.value) {
        !is Initial -> true.also { popState() }
        else -> false
    }

    private fun setInitialState(calculatedData: CalculatedDataEntity?, actions: List<TreeEntity>) {
        val initialSatellites = actions
                .filter { it.id != EXPENSE_CORE_ID }
                .map { ExpenseMapper.treeEntityToCircleData(it) }

        val initial = Initial(null, initialSatellites, core, null, "", "")
        stateStack.value = mutableListOf(initial)
        inputValue.value = calculatedData
    }

    private fun pushState(state: ExpenseStates) {
        stateStack.value?.add(state)
        stateStack.value = stateStack.value
    }

    private fun popState() {
        if (isLocked) return
        val states = stateStack.value ?: return
        if (states.size < 1) return
        val from = states.last().also { states.remove(it) }
        val to = states.last()
        states[states.lastIndex] = getPreviousState(from, to) ?: return
        stateStack.value = states
    }

    private fun getNextState(): ExpenseStates? {
        if (isLocked) return null
        return when (state.value) {
            is Initial -> getInputValueState()
            is InputValue -> getPickCategoryState()
            is PickCategory -> return getSavedInitialState(state.value)
                    .also { stateStack.value?.clear() }
            null -> throw IllegalAccessError()
        }
    }

    private fun getPreviousState(from: ExpenseStates, to: ExpenseStates): ExpenseStates? {
        if (isLocked) return null
        return when (to) {
            is Initial -> getSavedInitialState(from)
            is InputValue -> InputValue(from, core.copy(icon = getConfigCurrency()))
            is PickCategory -> PickCategory(from, pickCategorySatellites, to.core)
        }
    }

    private fun getPickCategoryState(): PickCategory {
        return PickCategory(state.value, pickCategorySatellites, core.copy(icon = ""))
    }

    private fun getInputValueState(): InputValue {
        inputValue.value = resetCalculatedData.execute()
        return InputValue(state.value, core.copy(icon = getConfigCurrency()))
    }

    private fun getConfigCurrency(): String {
        return config.find { it.type == ConfigType.CURRENCY }?.value ?: ""
    }

    private fun getSavedInitialState(from: ExpenseStates?): Initial {
        val state = stateStack.value?.get(0) as? Initial ?: throw IllegalAccessError()
        return Initial(from, state.satellites, state.core, null,
                inputValue.value?.displayValue ?: "", getConfigCurrency())
    }
}
