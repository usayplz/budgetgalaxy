package com.sayplz.budgetgalaxy.ui.system

/**
 * Created by Sergei Kurikalov
 * on 18.07.18.
 */
data class SystemViewSizes(
        val width: Int,
        val height: Int,
        val inputStateHeight: Int,
        val coreSize: Int,
        val orbitSize: Int,
        val extendedOrbitSize: Int
)
