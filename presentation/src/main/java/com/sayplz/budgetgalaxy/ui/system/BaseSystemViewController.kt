package com.sayplz.budgetgalaxy.ui.system


abstract class BaseSystemViewController {
    companion object {
        const val ANIMATION_DURATION_NORMAL = 500L
        const val ANIMATION_DURATION_MIN = 250L
    }
}