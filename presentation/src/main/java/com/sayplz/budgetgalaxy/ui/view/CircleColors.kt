package com.sayplz.budgetgalaxy.ui.view

import android.graphics.Color
import androidx.annotation.ColorInt

data class CircleColors(
        @ColorInt val body: Int = DEFAULT.body,
        @ColorInt val icon: Int = DEFAULT.icon,
        @ColorInt val title: Int = DEFAULT.title,
        @ColorInt val outerLineColor: Int = DEFAULT.outerLineColor,
        @ColorInt val innerLineColor: Int = DEFAULT.innerLineColor,
        @ColorInt val edgeColor: Int = DEFAULT.edgeColor,
        @ColorInt val edgeColor2: Int = DEFAULT.edgeColor2
) {
    companion object {
        val DEFAULT = CircleColors(
                Color.BLUE,
                Color.WHITE,
                Color.WHITE,
                Color.WHITE,
                Color.BLUE,
                Color.BLUE,
                Color.BLUE
        )
    }
}

