package com.sayplz.budgetgalaxy.ui.expense.system

import android.animation.Animator
import com.sayplz.budgetgalaxy.R
import com.sayplz.budgetgalaxy.ui.system.BaseSystemViewController
import com.sayplz.budgetgalaxy.ui.system.BaseViewController
import com.sayplz.budgetgalaxy.ui.system.SystemViewSizes
import com.sayplz.budgetgalaxy.ui.view.Circle
import com.sayplz.budgetgalaxy.ui.view.Core

class CoreViewController(var core: Core, var isExtendedState: Boolean, sizes: SystemViewSizes)
    : BaseViewController(sizes.width, sizes.height), ExpenseStatesInterface {

    private val duration: Long
    private val marginTiny: Int

    private val size: Int
    private val x: Float
    private val y: Float

    private val inputStateSize: Int
    private val inputStateX: Float
    private val inputStateY: Float

    private val extendedStateX: Float
    private val extendedStateY: Float

    init {
        duration = BaseSystemViewController.ANIMATION_DURATION_NORMAL
        marginTiny = core.context.resources.getDimensionPixelSize(R.dimen.space_tiny)

        size = sizes.coreSize
        x = center.x - size / 2f
        y = center.y - size / 2f

        inputStateSize = sizes.orbitSize - 2 * marginTiny
        inputStateX = center.x - inputStateSize / 2f
        inputStateY = Math.min(sizes.width, sizes.inputStateHeight) / 2f - inputStateSize / 2f

        extendedStateX = sizes.width - 2f * marginTiny - size
        extendedStateY = sizes.height - 2f * marginTiny - size

        core.updateParams(size, x, y)
    }

    override fun toInitialStateAnimator(): Animator? {
        core.state = Circle.State.DEFAULT
        val toX = if (isExtendedState) extendedStateX else x
        val toY = if (isExtendedState) extendedStateY else y
        return core.createMorphAnimator(size, toX, toY, duration)
    }

    override fun fromInitialToInputStateAnimator(): Animator? {
        core.bringToFront()
        core.state = Circle.State.TEXT_INPUT_VALUE
        return core.createMorphAnimator(inputStateSize, inputStateX, inputStateY, duration)
    }

    override fun fromInputValueToPickCategoryStateAnimator(): Animator? {
        return toInitialStateAnimator()
    }

    override fun fromInputValueToInitialStateAnimator(): Animator? {
        return toInitialStateAnimator()
    }

    override fun fromPickCategoryToInputStateAnimator(): Animator? {
        return fromInitialToInputStateAnimator()
    }

    override fun fromPickCategoryToPickCategoryStateAnimator(): Animator? {
        return toInitialStateAnimator()
    }

    override fun fromPickCategoryToInitialStateAnimator(): Animator? {
        core.bringToFront()
        return toInitialStateAnimator()
    }

    fun updateCore(newCore: Core) {
        core.apply {
            this.id = newCore.id
            this.text = newCore.text
            this.iconRes = newCore.iconRes
            this.iconText = newCore.iconText
            this.colors = newCore.colors
        }
        core.invalidate()
    }

    fun updateCore(newCore: Core, size: Int, x: Float, y: Float) {
        core.updateParams(size, x, y)
        updateCore(newCore)
    }
}
