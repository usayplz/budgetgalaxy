package com.sayplz.budgetgalaxy.util

fun Boolean.toInt(): Int {
    return if (this) 1 else 0
}