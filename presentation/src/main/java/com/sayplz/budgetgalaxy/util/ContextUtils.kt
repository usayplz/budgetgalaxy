package com.sayplz.budgetgalaxy.util

import android.app.Activity
import android.content.Context
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import android.view.View
import android.view.inputmethod.InputMethodManager

fun Context.dimen(@DimenRes resource: Int): Int = resources.getDimensionPixelSize(resource)

fun Fragment.dimen(@DimenRes resource: Int): Int = context?.dimen(resource) ?: 0

fun View.dimen(@DimenRes resource: Int): Int = context.dimen(resource)

fun Context.string(@StringRes resource: Int): String = getString(resource)

fun Fragment.string(@StringRes resource: Int): String = getString(resource)

fun Context.color(@ColorRes resource: Int): Int = ContextCompat.getColor(this, resource)

fun Activity.hideKeyboard() {
    val view = this.currentFocus ?: return
    with(getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager) {
        hideSoftInputFromWindow(view.windowToken, 0)
    }
}

fun Context.showKeyboard() {
    with(getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager) {
        toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }
}

