package com.sayplz.budgetgalaxy.util

import android.app.Activity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import androidx.viewpager.widget.ViewPager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


inline fun <reified T> RecyclerView.init(
        adapter: ListAdapter<T, RecyclerView.ViewHolder>,
        layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context),
        itemAnimator: RecyclerView.ItemAnimator = DefaultItemAnimator(),
        hasFixedSize: Boolean = true) {
    this.adapter = adapter
    this.layoutManager = layoutManager
    this.itemAnimator = itemAnimator
    setHasFixedSize(hasFixedSize)
}

fun View.visible(isVisible: Boolean) {
    this.visibility = if (isVisible) View.VISIBLE else View.GONE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.INVISIBLE
}

fun ViewGroup?.inflate(layoutRes: Int, root: ViewGroup? = null, attacheToRoot: Boolean = false): View {
    return LayoutInflater.from(this?.context).inflate(layoutRes, root, attacheToRoot)
}

fun <V : View?> BottomSheetBehavior<V>.setCallbacks(
        onSlide: (View, Float) -> Unit = { _, _ -> },
        onStateChanged: (View, Int) -> Unit = { _, _ -> }) {
    this.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
        override fun onSlide(bottomSheet: View, slideOffset: Float) {
            onSlide(bottomSheet, slideOffset)
        }

        override fun onStateChanged(bottomSheet: View, newState: Int) {
            onStateChanged(bottomSheet, newState)
        }
    })
}

fun ViewPager.onPageChangeListener(
        onPageSelected: (Int) -> Unit = { _ -> },
        onPageScrolled: (Int, Float, Int) -> Unit = { _, _, _ -> },
        onPageScrollStateChanged: (Int) -> Unit = { _ -> }) {
    this.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
        override fun onPageSelected(position: Int) {
            onPageSelected(position)
        }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            onPageScrolled(position, positionOffset, positionOffsetPixels)
        }

        override fun onPageScrollStateChanged(state: Int) {
            onPageScrollStateChanged(state)
        }
    })
}

fun View.setMargins(left: Int? = null, top: Int? = null, right: Int? = null, bottom: Int? = null) {
    this.layoutParams = (this.layoutParams as ViewGroup.MarginLayoutParams).apply {
        setMargins(left ?: leftMargin, top ?: topMargin,
                right ?: rightMargin, bottom ?: bottomMargin)
    }
}

fun Activity?.getDisplayWidth(): Int {
    val metrics = DisplayMetrics()
    this?.windowManager?.defaultDisplay?.getMetrics(metrics)
    return metrics.widthPixels
}

fun Activity?.getDisplayHeight(): Int {
    val metrics = DisplayMetrics()
    this?.windowManager?.defaultDisplay?.getMetrics(metrics)
    return metrics.heightPixels
}
