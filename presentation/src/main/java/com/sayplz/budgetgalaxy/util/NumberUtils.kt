package com.sayplz.budgetgalaxy.util

import java.math.BigDecimal
import java.text.NumberFormat

fun BigDecimal.asPrice(): String {
    val formatter = NumberFormat.getCurrencyInstance()
    return formatter.format(this)
}