package com.sayplz.budgetgalaxy.util

import android.util.Log

private const val TAG = "INFO"

fun Any.info(output: Any?, exception: Exception? = null) {
    if (Log.isLoggable(TAG, Log.INFO)) {
        Log.i(TAG, output?.toString() ?: "null", exception)
    }
}