package com.sayplz.budgetgalaxy.util

import androidx.annotation.CheckResult
import com.sayplz.budgetgalaxy.common.UiResult
import com.sayplz.domain.base.Schedulers
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

@CheckResult
fun <T> Flowable<T>.toResult(schedulers: Schedulers):
        Flowable<UiResult<T>> {
    return compose { item ->
        item
                .map { UiResult.success(it) }
                .onErrorReturn { e -> UiResult.failure(e.message ?: "unknown", e) }
                .observeOn(schedulers.ui)
                .startWith(UiResult.inProgress())
    }
}

@CheckResult
fun <T> Observable<T>.toResult(schedulers: Schedulers):
        Observable<UiResult<T>> {
    return compose { item ->
        item
                .map { UiResult.success(it) }
                .onErrorReturn { e -> UiResult.failure(e.message ?: "unknown", e) }
                .observeOn(schedulers.ui)
                .startWith(UiResult.inProgress())
    }
}

@CheckResult
fun <T> Single<T>.toResult(schedulers: Schedulers):
        Observable<UiResult<T>> {
    return toObservable().toResult(schedulers)
}

@CheckResult
fun <T> Completable.toResult(schedulers: Schedulers):
        Observable<UiResult<T>> {
    return toObservable<T>().toResult(schedulers)
}