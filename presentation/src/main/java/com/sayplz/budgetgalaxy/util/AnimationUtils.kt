package com.sayplz.budgetgalaxy.util

import android.animation.Animator

fun Animator.addOnAnimationEnd(onAnimationEnd: () -> Unit) {
    this.addListener(object : Animator.AnimatorListener {
        override fun onAnimationRepeat(animation: Animator?) {}
        override fun onAnimationCancel(animation: Animator?) {}
        override fun onAnimationStart(animation: Animator?) {}
        override fun onAnimationEnd(animation: Animator?) {
            onAnimationEnd()
        }
    })
}

fun Animator.addOnAnimationStart(onAnimationStart: () -> Unit) {
    this.addListener(object : Animator.AnimatorListener {
        override fun onAnimationRepeat(animation: Animator?) {}
        override fun onAnimationCancel(animation: Animator?) {}
        override fun onAnimationEnd(animation: Animator?) {}
        override fun onAnimationStart(animation: Animator?) {
            onAnimationStart()
        }
    })
}

fun Animator.addOnAnimationRepeat(onAnimationRepeat: () -> Unit) {
    this.addListener(object : Animator.AnimatorListener {
        override fun onAnimationCancel(animation: Animator?) {}
        override fun onAnimationEnd(animation: Animator?) {}
        override fun onAnimationStart(animation: Animator?) {}
        override fun onAnimationRepeat(animation: Animator?) {
            onAnimationRepeat()
        }
    })
}