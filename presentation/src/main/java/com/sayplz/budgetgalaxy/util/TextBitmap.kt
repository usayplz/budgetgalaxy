package com.sayplz.budgetgalaxy.util

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.text.TextPaint

class TextBitmap private constructor(private val textColor: Int, private val textSize: Int,
                                     private val bitmapSize: Int) {

    private val textPaint = TextPaint().apply {
        isAntiAlias = true
        textSize = this@TextBitmap.textSize.toFloat()
        color = textColor
        style = Paint.Style.FILL
    }

    fun create(text: String, count: Int = 1): Bitmap {
        val formatted = text.lettersOnly().take(count).toUpperCase()
        val image = Bitmap.createBitmap(bitmapSize * 2, bitmapSize, Bitmap.Config.ARGB_8888)

        val canvas = Canvas(image)
        createText(formatted, canvas)

        return image
    }

    private fun String.lettersOnly(): String {
        val re = Regex("[^\\w]")
        return re.replace(this, "")
    }

    private fun createText(text: String, canvas: Canvas) {
        val x = (bitmapSize * 2 - textPaint.measureText(text)) / 2f
        val y = (bitmapSize - (textPaint.descent() + textPaint.ascent())) / 2f
        canvas.drawText(text, x, y, textPaint)
    }

    companion object {
        @Volatile
        private var INSTANCE: TextBitmap? = null

        fun getInstance(textColor: Int, textSize: Int, bitmapSize: Int): TextBitmap {
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: TextBitmap(textColor, textSize, bitmapSize)
                        .also { INSTANCE = it }
            }
        }
    }
}
