package com.sayplz.budgetgalaxy.util

import android.content.Context
import android.graphics.Color
import androidx.core.content.ContextCompat
import java.util.*

private val COLORS = listOf("red", "pink_", "purple_", "dark_purple_", "indigo_",
        "blue_", "light_blue_", "cyan_", "teal_", "green_", "light_green_", "lime_", "yellow_",
        "amber_", "orange_", "deep_orange_")
private val HUES = listOf("50", "100", "200", "300", "400", "500", "600", "700", "800", "900", "A100", "A200", "A400", "A700")
private val random = Random()

fun Context.getRandomHueWithColor(color: String): Int {
    val colorName = color + HUES[random.nextInt(HUES.size)]
    try {
        return ContextCompat.getColor(this, resources.getIdentifier(colorName, "color", this.packageName))
    } catch (e: Exception) {
        return Color.RED
    }
}

fun Context.getRandomColor(): Int {
    return this.getRandomHueWithColor(COLORS[random.nextInt(COLORS.size)])
}
