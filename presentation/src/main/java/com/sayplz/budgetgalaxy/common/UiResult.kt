package com.sayplz.budgetgalaxy.common

sealed class UiResult<T>(val inProgress: Boolean) {

    class InProgress<T> : UiResult<T>(true) {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false
            return true
        }

        override fun hashCode(): Int = javaClass.hashCode()
    }

    data class Success<T>(var data: T) : UiResult<T>(false)
    data class Failure<T>(val errorMessage: String?, val e: Throwable) : UiResult<T>(false)
    companion object {
        fun <T> inProgress(): UiResult<T> = InProgress()

        fun <T> success(data: T): UiResult<T> = Success(data)

        fun <T> failure(errorMessage: String, e: Throwable): UiResult<T> = Failure(errorMessage, e)
    }
}