package com.sayplz.budgetgalaxy.common

import com.sayplz.domain.base.Schedulers
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers.io

class AppSchedules : Schedulers {
    override val io: Scheduler
        get() = io()
    override val ui: Scheduler
        get() = AndroidSchedulers.mainThread()
}
