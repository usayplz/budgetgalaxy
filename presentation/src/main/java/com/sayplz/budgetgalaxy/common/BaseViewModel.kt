package com.sayplz.budgetgalaxy.common

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel : ViewModel() {
    open val subscription = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        subscription.clear()
    }
}