package com.sayplz.budgetgalaxy

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.sayplz.budgetgalaxy.di.appModule
import org.koin.android.ext.android.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        setupVectorDrawable()
        setupKoin()
    }

    private fun setupKoin() {
        startKoin(this, listOf(appModule))
    }

    private fun setupVectorDrawable() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }
}