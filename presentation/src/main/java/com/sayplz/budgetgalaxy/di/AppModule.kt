package com.sayplz.budgetgalaxy.di

import android.content.Context
import androidx.room.Room
import com.sayplz.budgetgalaxy.common.AppSchedules
import com.sayplz.budgetgalaxy.ui.expense.ExpenseViewModel
import com.sayplz.data.common.Constants
import com.sayplz.data.common.InitialDatabaseData
import com.sayplz.data.local.ConfigLocalDataSource
import com.sayplz.data.local.DiskDatabase
import com.sayplz.data.local.TreeLocalDataSource
import com.sayplz.data.local.dao.ConfigDao
import com.sayplz.data.local.dao.TreeDao
import com.sayplz.data.repository.ConfigRepositoryImpl
import com.sayplz.data.repository.ExpenseRepositoryImpl
import com.sayplz.domain.base.Schedulers
import com.sayplz.domain.repository.ConfigRepostory
import com.sayplz.domain.repository.ExpenseRepository
import com.sayplz.domain.usecase.*
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val appModule = module {
    single(name = Providers.DATABASE) { provideDatabase(androidContext()) }
    single { AppSchedules() as Schedulers }

    factory { (get(Providers.DATABASE) as DiskDatabase).getExpenseDao() } bind TreeDao::class
    factory { (get(Providers.DATABASE) as DiskDatabase).getConfigDao() } bind ConfigDao::class

    factory { TreeLocalDataSource(get()) }
    factory { ConfigLocalDataSource(get()) }
    single { ExpenseRepositoryImpl(get()) } bind ExpenseRepository::class
    single { ConfigRepositoryImpl(get()) } bind ConfigRepostory::class

    module(Modules.EXPENSE) {
        factory { AddKeyUseCase(get()) }
        factory { ResetCalculatedDataUseCase(get()) }
        factory { GetExpenseCoreUseCase(get(), get()) }
        factory { GetExpensesByParentIdUseCase(get(), get()) }
        factory { GetConfigUseCase(get(), get()) }
        viewModel { ExpenseViewModel(get(), get(), get(), get(), get(), get()) }
    }
}

private fun provideDatabase(context: Context): DiskDatabase = Room
        .databaseBuilder(context, DiskDatabase::class.java, Constants.DATABASE_NAME)
        .addCallback(InitialDatabaseData(context).onCreateCallback())
        .fallbackToDestructiveMigration()
        .build()

private object Providers {
    const val DATABASE = "database"
}

private object Modules {
    const val EXPENSE = "expense"
}
