package com.sayplz.data.calculator

import junit.framework.Assert
import org.junit.Test
import java.math.BigDecimal

class CalculatorTest {
    @Test
    fun calc() {
        var expression = ""
        var expected: BigDecimal? = null
        var actual: BigDecimal? = Calculator.calc(expression)
        Assert.assertEquals("Empty string", expected, actual)

        expression = "234.34"
        expected = expression.toBigDecimal()
        actual = Calculator.calc(expression)
        Assert.assertEquals("Just value", expected, actual)

        expression = "4567567"
        expected = expression.toBigDecimal()
        actual = Calculator.calc(expression)
        Assert.assertEquals("Just value 1", expected, actual)

        expression = "234.34+34.65+20"
        expected = "288.99".toBigDecimal()
        actual = Calculator.calc(expression)
        Assert.assertEquals("Expression plus", expected, actual)

        expression = "234.34-34.65+20"
        expected = "219.69".toBigDecimal()
        actual = Calculator.calc(expression)
        Assert.assertEquals("Expression plus", expected, actual)

        expression = "234.34-34.65*20-2/5+12*3-44/4"
        expected = "-434.06".toBigDecimal()
        actual = Calculator.calc(expression)
        Assert.assertEquals("Expression plus", expected, actual)

        expression = "54/0"
        expected = null
        actual = Calculator.calc(expression)
        Assert.assertEquals("Expression plus", expected, actual)
    }
}