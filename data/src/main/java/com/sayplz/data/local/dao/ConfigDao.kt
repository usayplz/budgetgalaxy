package com.sayplz.data.local.dao

import androidx.room.*
import com.sayplz.data.local.model.ConfigLocal
import io.reactivex.Flowable
import io.reactivex.Maybe

@Dao
interface ConfigDao {
    @Query("SELECT * FROM config WHERE type = :configType")
    fun getValue(configType: String): Maybe<ConfigLocal>

    @Query("SELECT * FROM config")
    fun getConfig(): Flowable<List<ConfigLocal>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(config: ConfigLocal)

    @Query("UPDATE config SET value = :value WHERE type = :type")
    fun updateType(type: String, value: String)

    @Update
    fun update(config: ConfigLocal)
}
