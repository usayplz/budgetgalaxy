package com.sayplz.data.local.utils

import androidx.room.TypeConverter
import com.sayplz.domain.entity.ConfigType
import com.sayplz.domain.entity.GenderEntity
import com.sayplz.domain.entity.TreeStatus
import com.sayplz.domain.entity.TreeType
import java.util.*

class Converters {
    @TypeConverter
    fun timestampToDate(millisSinceEpoch: Long?): Date? {
        return millisSinceEpoch?.toDate()
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

    @TypeConverter
    fun treeTypeToString(treeType: TreeType): String {
        return treeType.name
    }

    @TypeConverter
    fun stringToTreeType(name: String): TreeType {
        return TreeType.valueOf(name)
    }

    @TypeConverter
    fun treeStatusToString(treeStatus: TreeStatus): String {
        return treeStatus.name
    }

    @TypeConverter
    fun stringToTreeStatus(name: String): TreeStatus {
        return TreeStatus.valueOf(name)
    }

    @TypeConverter
    fun genderToString(gender: GenderEntity): String {
        return gender.name
    }

    @TypeConverter
    fun stringToGender(name: String): GenderEntity {
        return GenderEntity.valueOf(name)
    }

    @TypeConverter
    fun configTypeToString(configType: ConfigType): String {
        return configType.name
    }

    @TypeConverter
    fun stringToConfigType(name: String): ConfigType {
        return ConfigType.valueOf(name)
    }

    private fun Long.toDate() = Date(this)
}
