package com.sayplz.data.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.sayplz.domain.entity.TreeStatus
import com.sayplz.domain.entity.TreeType
import java.util.*

@Entity(tableName = "tree")
data class TreeLocal(
        @PrimaryKey val id: Long,
        val type: TreeType,
        val parentId: Long,
        val term: String,
        val customTerm: String,
        val fontSize: Int,
        val icon: String,
        val bodyColor: String,
        val iconColor: String,
        val titleColor: String,
        val outerLineColor: String,
        val innerLineColor: String,
        val edgeColor: String,
        val edgeColor2: String,
        val status: TreeStatus,
        val createDate: Date,
        val updateDate: Date
)
