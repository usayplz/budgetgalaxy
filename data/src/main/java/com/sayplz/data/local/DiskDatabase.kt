package com.sayplz.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.sayplz.data.common.Constants
import com.sayplz.data.local.dao.ConfigDao
import com.sayplz.data.local.dao.TreeDao
import com.sayplz.data.local.model.ConfigLocal
import com.sayplz.data.local.model.TreeLocal
import com.sayplz.data.local.utils.Converters

/**
 * Created by Sergei Kurikalov
 * on 19.03.18.
 */

@Database(entities = [TreeLocal::class, ConfigLocal::class], version = Constants.DATABASE_VERSION)
@TypeConverters(Converters::class)
abstract class DiskDatabase : RoomDatabase() {
    abstract fun getExpenseDao(): TreeDao
    abstract fun getConfigDao(): ConfigDao
}
