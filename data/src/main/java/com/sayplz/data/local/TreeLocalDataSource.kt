package com.sayplz.data.local

import com.sayplz.data.local.dao.TreeDao
import com.sayplz.data.local.model.TreeLocal
import io.reactivex.Maybe

class TreeLocalDataSource(private val treeDao: TreeDao) {

    fun getTreeById(id: Long, treeType: String): Maybe<TreeLocal> {
        return treeDao.getTreeById(id, treeType)
    }

    fun getTreeWithParentId(parentId: Long, treeType: String): Maybe<List<TreeLocal>> {
        return treeDao.getTreeWithParentId(parentId, treeType)
    }
}
