package com.sayplz.data.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.sayplz.domain.entity.ConfigType

@Entity(tableName = "config")
data class ConfigLocal(
        @PrimaryKey(autoGenerate = true) val id: Long,
        val type: ConfigType,
        val value: String
)