package com.sayplz.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.sayplz.data.local.model.TreeLocal
import io.reactivex.Maybe

/**
 * Created by Sergei Kurikalov
 * on 21.06.18.
 */

@Dao
interface TreeDao {
    @Query("SELECT * FROM tree WHERE parentId = :parentId and type = :type")
    fun getTreeWithParentId(parentId: Long, type: String): Maybe<List<TreeLocal>>

    @Query("SELECT * FROM tree WHERE id = :id and type = :type")
    fun getTreeById(id: Long, type: String): Maybe<TreeLocal>
}
