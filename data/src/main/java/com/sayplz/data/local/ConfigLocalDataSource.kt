package com.sayplz.data.local

import com.sayplz.data.local.dao.ConfigDao
import com.sayplz.data.local.model.ConfigLocal
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe

class ConfigLocalDataSource(private val configDao: ConfigDao) {
    fun getValue(configType: String): Maybe<ConfigLocal> {
        return configDao.getValue(configType)
    }

    fun getConfig(): Flowable<List<ConfigLocal>> {
        return configDao.getConfig()
    }

    fun insert(config: ConfigLocal): Completable {
        return Completable.fromCallable { configDao.insert(config) }
    }

    fun update(configType: String, value: String): Completable {
        return Completable.fromCallable { configDao.updateType(configType, value) }
    }
}