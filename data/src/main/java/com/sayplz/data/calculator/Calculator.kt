package com.sayplz.data.calculator

import java.math.BigDecimal

object Calculator {
    fun calc(expression: String) = when {
        expression.isEmpty() -> null
        expression.isExpression() -> tryEvalOrNull(expression)
        else -> expression.toBigDecimal()
    }

    private fun tryEvalOrNull(expression: String) = try {
        eval(expression)
    } catch (e: Exception) {
        null
    }

    private fun eval(expression: String): BigDecimal? {
        val tokens: MutableList<String> = getTokens(expression)

        do {
            val i = checkTimes(tokens)
            val j = checkDivide(tokens)
        } while (i >= 0 || j >= 0)

        var i = 1
        while (tokens.size > 1) {
            when (tokens[i]) {
                "+" -> tokens[i - 1] = doOperation(tokens[i - 1], tokens[i + 1], "+")
                        .also { removeEnds(tokens, i) }
                "-" -> tokens[i - 1] = doOperation(tokens[i - 1], tokens[i + 1], "-")
                        .also { removeEnds(tokens, i) }
                else -> i += 2
            }
        }
        return tokens[0].toBigDecimal()
    }

    private fun checkDivide(tokens: MutableList<String>): Int {
        val j = tokens.indexOf("/")
        if (j >= 0) {
            tokens[j - 1] = doOperation(tokens[j - 1], tokens[j + 1], "/")
            removeEnds(tokens, j)
        }
        return j
    }

    private fun checkTimes(tokens: MutableList<String>): Int {
        val i = tokens.indexOf("*")
        if (i >= 0) {
            tokens[i - 1] = doOperation(tokens[i - 1], tokens[i + 1], "*")
            removeEnds(tokens, i)
        }
        return i
    }

    private fun removeEnds(tokens: MutableList<String>, i: Int) {
        tokens.removeAt(i + 1)
        tokens.removeAt(i)
    }

    private fun getTokens(expression: String): MutableList<String> {
        val tokens = mutableListOf<String>()
        var digits = ""
        expression.forEach {
            if (it == '+' || it == '-' || it == '*' || it == '/') {
                tokens.add(digits)
                tokens.add(it.toString())
                digits = ""
            } else {
                digits += it.toString()
            }
        }
        if (digits.isNotEmpty()) tokens.add(digits)
        return tokens
    }

    private fun doOperation(val1: String, val2: String, operator: String) = when (operator) {
        "+" -> val1.toBigDecimal().plus(val2.toBigDecimal()).toPlainString()
        "-" -> val1.toBigDecimal().minus(val2.toBigDecimal()).toPlainString()
        "*" -> val1.toBigDecimal().times(val2.toBigDecimal()).toPlainString()
        "/" -> val1.toBigDecimal().divide(val2.toBigDecimal()).toPlainString()
        else -> ""
    }
}
