package com.sayplz.data.calculator

import com.sayplz.domain.entity.CalculatedDataEntity
import com.sayplz.domain.entity.getDecimalSeparator
import com.sayplz.domain.entity.KeyboardKeyEntity
import com.sayplz.domain.entity.KeyboardKeyEntity.*
import com.sayplz.domain.entity.getThousandSeparator
import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.NumberFormat

class CalculatorDataSource {
    companion object {
        private const val EXPRESSION_LENGTH_LIMIT = 32
        private const val PRICE_LENGTH_LIMIT = 13
    }

    private var calculatedData = getEmptyData()

    fun addKey(key: KeyboardKeyEntity): CalculatedDataEntity {
        if (isDataLimited() && key != BACKSPACE && key != LONG_BACKSPACE) {
            return calculatedData
        }

        var expression = getExpression(key)
        val value: BigDecimal? = getValue(expression)
        val displayValue = getDisplayValue(expression, value)

        if (!expression.isExpression()) {
            expression = ""
        }

        calculatedData = CalculatedDataEntity(value, displayValue, expression)
        return calculatedData
    }

    fun clear(): CalculatedDataEntity {
        calculatedData = getEmptyData()
        return calculatedData
    }

    private fun getValue(expression: String) = when {
        expression.isExpression() -> calc(expression)
        expression.isEmpty() -> null
        else -> clean(expression).toBigDecimal()
    }

    private fun getDisplayValue(exp: String, value: BigDecimal?): String {
        val comma = if (exp.lastOrNull()?.toString() == COMMA.tag) COMMA.tag else ""
        return when (exp.isExpression()) {
            true -> value?.asPrice() ?: ""
            false -> value?.asString()?.plus(comma) ?: ""
        }
    }

    private fun getExpression(key: KeyboardKeyEntity): String {
        val expression = when (calculatedData.expression.isEmpty()) {
            true -> clean(calculatedData.displayValue)
            false -> calculatedData.expression
        }
        return transformExpressionWithKey(key, expression)
    }

    private fun isDataLimited(): Boolean {
        return calculatedData.expression.length > EXPRESSION_LENGTH_LIMIT
                || calculatedData.displayValue.length > PRICE_LENGTH_LIMIT
    }

    private fun transformExpressionWithKey(key: KeyboardKeyEntity, expression: String) = when (key) {
        KEY7, KEY8, KEY9, KEY4, KEY5, KEY6, KEY1, KEY2, KEY3 -> addDigit(expression, key.tag)
        KEY0 -> addZero(expression)
        COMMA -> addComma(expression)
        DIVIDE, MULTIPLY, MINUS, PLUS -> addOperator(expression, key.tag)
        BACKSPACE -> removeLast(expression)
        LONG_BACKSPACE -> clear().expression
        else -> expression
    }

    private fun removeLast(expression: String) = when {
        expression.isEndWithOperator() -> expression.dropLast(3)
        expression.isNotEmpty() -> expression.dropLast(1)
        else -> expression
    }

    private fun addOperator(expression: String, operator: String) = when {
        expression.isEndWithOperator() -> expression.dropLast(3).plus(" $operator ")
        expression.isEmpty() -> ""
        expression.endsWith(".") || expression.endsWith(",") -> expression.dropLast(1).plus(" $operator ")
        else -> expression.plus(" $operator ")
    }

    private fun addComma(expression: String) = when {
        expression.getEnd().contains(COMMA.tag) -> expression
        expression.getEnd().isEmpty() -> "0${COMMA.tag}"
        else -> expression.plus(COMMA.tag)
    }

    private fun addDigit(expression: String, digit: String) = if (expression.getEnd() == "0") {
        expression.dropLast(1).plus(digit)
    } else {
        expression.plus(digit)
    }

    private fun addZero(expression: String) = when (expression != "0") {
        true -> expression.plus(KEY0.tag)
        false -> expression
    }

    private fun calc(expression: String): BigDecimal? = try {
        val clean = clean(expression)
        Calculator.calc(clean)
    } catch (e: Exception) {
        null
    }


    private fun getEmptyData(): CalculatedDataEntity {
        return CalculatedDataEntity(null, "", "")
    }

    private fun clean(expression: String): String {
        val cleaned = expression
                .replace(" ", "")
                .replace("÷", "/")
                .replace("×", "*")
                .replace(getDecimalSeparator(), ".")
                .replace(getThousandSeparator(), "")

        return if (Regex(".*[+\\-/*]+$").matches(cleaned)) {
            cleaned.dropLast(1)
        } else {
            cleaned
        }
    }

    private fun String.isEndWithOperator(): Boolean {
        return this.endsWith(" ")
    }

    private fun String.getEnd(): String = if (this.isExpression()) {
        this.substring(this.lastIndexOf(" ") + 1, this.length)
    } else {
        this
    }

    private fun BigDecimal.asPrice(): String {
        return NumberFormat.getNumberInstance().apply {
            minimumFractionDigits = 0
            maximumFractionDigits = 2
        }.format(this)
    }

    private fun BigDecimal.asString(): String {
        return DecimalFormat().apply {
            minimumFractionDigits = 0
            maximumFractionDigits = 5
        }.format(this)
    }
}
