package com.sayplz.data.calculator

fun String.isExpression(): Boolean {
    return this.matches(Regex(".*[+\\-÷×/*]+.*"))
}