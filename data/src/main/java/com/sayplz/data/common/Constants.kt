package com.sayplz.data.common

object Constants {
    const val DATABASE_NAME = "app.db"
    const val DATABASE_VERSION = 1

    const val EXPENSE_CORE_ID = 1L
}