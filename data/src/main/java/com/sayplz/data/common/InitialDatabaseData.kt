package com.sayplz.data.common

import android.content.Context
import androidx.annotation.StringRes
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.sayplz.data.local.model.TreeLocal
import com.sayplz.domain.entity.ConfigType
import com.sayplz.domain.entity.TreeStatus
import com.sayplz.domain.entity.TreeType
import java.util.*

class InitialDatabaseData(private val context: Context) {
    fun onCreateCallback(): RoomDatabase.Callback = object : RoomDatabase.Callback() {
        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            fill(db)
        }
    }

    private fun fill(db: SupportSQLiteDatabase) {
        getExpenses().map { insert(it) }.forEach { db.execSQL(it) }
        createConfig(db)
    }

    private fun createConfig(db: SupportSQLiteDatabase) {
        val currencyCode = Currency.getInstance(Locale.getDefault()).currencyCode
        db.execSQL("INSERT INTO config VALUES(0, '${ConfigType.CURRENCY.name}', '$currencyCode')")
    }

    private fun getExpenses(): List<TreeLocal> {
        val expense = getExpenseCore()

        return listOf(
                expense,

                expense.copy(id = 2, parentId = 0, term = "settings", icon = "ic_settings",
                        bodyColor = "#B5B4B6", iconColor = "#484748", titleColor = "#484748",
                        innerLineColor = "#B5B4B6", outerLineColor = "#B5B4B6", edgeColor = "#484748",
                        edgeColor2 = "#484748"),

                // Fun
                expense.copy(id = 101, parentId = 1, term = "expense_fun",
                        icon = "expense_fun", bodyColor = "#E68782",
                        innerLineColor = "#E68782", outerLineColor = "#E68782"),

                expense.copy(id = 102, parentId = 101, term = "expense_coffee",
                        icon = "expense_coffee", bodyColor = "#E68782",
                        innerLineColor = "#E68782", outerLineColor = "#E68782"),

                expense.copy(id = 103, parentId = 101, term = "expense_cinema",
                        icon = "expense_cinema", bodyColor = "#E68782",
                        innerLineColor = "#E68782", outerLineColor = "#E68782"),

                expense.copy(id = 104, parentId = 101, term = "expense_fun_other",
                        icon = "expense_other", bodyColor = "#E68782",
                        innerLineColor = "#E68782", outerLineColor = "#E68782"),

                // Family
                expense.copy(id = 105, parentId = 1, term = "expense_family",
                        icon = "expense_family", bodyColor = "#988EE3",
                        innerLineColor = "#988EE3", outerLineColor = "#988EE3"),

                expense.copy(id = 106, parentId = 105, term = "expense_family_other",
                        icon = "expense_other", bodyColor = "#988EE3",
                        innerLineColor = "#988EE3", outerLineColor = "#988EE3"),

                // Health
                expense.copy(id = 107, parentId = 1, term = "expense_fun",
                        icon = "expense_fun", bodyColor = "#67D7C4",
                        innerLineColor = "#67D7C4", outerLineColor = "#67D7C4"),

                // Transport
                expense.copy(id = 108, parentId = 1, term = "expense_transport",
                        icon = "expense_transport", bodyColor = "#7AA3E5",
                        innerLineColor = "#7AA3E5", outerLineColor = "#7AA3E5"),

                // Home
                expense.copy(id = 109, parentId = 1, term = "expense_home",
                        icon = "expense_home", bodyColor = "#EBD95F",
                        innerLineColor = "#EBD95F", outerLineColor = "#EBD95F"),

                // Clothes
                expense.copy(id = 110, parentId = 1, term = "expense_clothes",
                        icon = "expense_clothes", bodyColor = "#EFA670",
                        innerLineColor = "#EFA670", outerLineColor = "#EFA670"),

                // Food
                expense.copy(id = 111, parentId = 1, term = "expense_food",
                        icon = "expense_food", bodyColor = "#9ED56B",
                        innerLineColor = "#9ED56B", outerLineColor = "#9ED56B"),

                // Other
                expense.copy(id = 112, parentId = 1, term = "expense_other",
                        icon = "expense_other", bodyColor = "#988EE3",
                        innerLineColor = "#988EE3", outerLineColor = "#988EE3")
        )
    }

    private fun getExpenseCore(): TreeLocal {
        return TreeLocal(1, TreeType.EXPENSE, 0, "expense", "",
                16, "ic_expense", "#E68782", "#585D61",
                "#585D61", "#585D61", "#E68782",
                "#E68782", "#E68782", TreeStatus.ACTIVE, Date(), Date())
    }

    private fun string(@StringRes resourceId: Int): String {
        return context.getString(resourceId)
    }

    private fun insert(tree: TreeLocal): String {
        return "INSERT INTO tree VALUES(${tree.id}, '${tree.type.name}', ${tree.parentId}, " +
                "'${tree.term}', '${tree.customTerm}', ${tree.fontSize}, '${tree.icon}', " +
                "'${tree.bodyColor}', '${tree.iconColor}', '${tree.titleColor}'," +
                "'${tree.outerLineColor}', '${tree.innerLineColor}', '${tree.edgeColor}'," +
                "'${tree.edgeColor2}', '${tree.status.name}', ${tree.createDate.time}, " +
                "${tree.updateDate.time} )"
    }
}
