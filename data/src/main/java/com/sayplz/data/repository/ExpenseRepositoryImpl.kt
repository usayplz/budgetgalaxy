package com.sayplz.data.repository

import com.sayplz.data.calculator.CalculatorDataSource
import com.sayplz.data.common.Constants.EXPENSE_CORE_ID
import com.sayplz.data.local.TreeLocalDataSource
import com.sayplz.data.mapper.TreeLocalToEntity
import com.sayplz.domain.entity.CalculatedDataEntity
import com.sayplz.domain.entity.KeyboardKeyEntity
import com.sayplz.domain.entity.TreeEntity
import com.sayplz.domain.entity.TreeType
import com.sayplz.domain.repository.ExpenseRepository
import io.reactivex.Maybe
import io.reactivex.Single

class ExpenseRepositoryImpl(private val treeLocalDataSource: TreeLocalDataSource) : ExpenseRepository {
    private val calculator = CalculatorDataSource()
    private val expenseType = TreeType.EXPENSE.name

    override fun resetCalculatedData(): CalculatedDataEntity {
        return calculator.clear()
    }

    override fun addKey(key: KeyboardKeyEntity): CalculatedDataEntity {
        return calculator.addKey(key)
    }

    override fun getExpenseCore(): Single<TreeEntity> {
        return treeLocalDataSource.getTreeById(EXPENSE_CORE_ID, expenseType)
                .toSingle()
                .map { TreeLocalToEntity.map(it) }
    }

    override fun getExpensesByParentId(parentId: Long): Maybe<List<TreeEntity>> {
        return treeLocalDataSource.getTreeWithParentId(parentId, expenseType)
                .map { expenses -> expenses.map { TreeLocalToEntity.map(it) } }
    }
}
