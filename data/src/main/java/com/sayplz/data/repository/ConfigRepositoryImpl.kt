package com.sayplz.data.repository

import com.sayplz.data.local.ConfigLocalDataSource
import com.sayplz.data.mapper.ConfigMapper
import com.sayplz.domain.entity.ConfigEntity
import com.sayplz.domain.entity.ConfigType
import com.sayplz.domain.repository.ConfigRepostory
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe

class ConfigRepositoryImpl(private val configLocalDataSource: ConfigLocalDataSource) : ConfigRepostory {
    override fun getValue(configType: ConfigType): Maybe<String> {
        return configLocalDataSource.getValue(configType.name)
                .map { it.value }
    }

    override fun getConfig(): Flowable<List<ConfigEntity>> {
        return configLocalDataSource.getConfig()
                .map { config -> config.map { ConfigMapper.map(it) } }
    }

    override fun insert(config: ConfigEntity): Completable {
        return configLocalDataSource.insert(ConfigMapper.unmap(config))
    }

    override fun update(type: ConfigType, value: String): Completable {
        return configLocalDataSource.update(type.name, value)
    }
}