package com.sayplz.data.mapper

import com.sayplz.data.local.model.TreeLocal
import com.sayplz.domain.entity.TreeEntity

/**
 * Created by Sergei Kurikalov
 * on 21.06.18.
 */

object TreeLocalToEntity : DataMapper<TreeLocal, TreeEntity> {
    override fun map(source: TreeLocal): TreeEntity {
        return TreeEntity(
                source.id,
                source.type,
                source.parentId,
                source.term,
                source.customTerm,
                source.fontSize,
                source.icon,
                source.bodyColor,
                source.iconColor,
                source.titleColor,
                source.outerLineColor,
                source.innerLineColor,
                source.edgeColor,
                source.edgeColor2,
                source.status,
                source.createDate,
                source.updateDate
        )
    }

    override fun unmap(source: TreeEntity): TreeLocal {
        return TreeLocal(
                source.id,
                source.type,
                source.parentId,
                source.term,
                source.customTerm,
                source.fontSize,
                source.icon,
                source.bodyColor,
                source.iconColor,
                source.titleColor,
                source.outerLineColor,
                source.innerLineColor,
                source.edgeColor,
                source.edgeColor2,
                source.status,
                source.createDate,
                source.updateDate
        )
    }

}