package com.sayplz.data.mapper

import com.sayplz.data.local.model.ConfigLocal
import com.sayplz.domain.entity.ConfigEntity

object ConfigMapper : DataMapper<ConfigLocal, ConfigEntity> {
    override fun map(source: ConfigLocal): ConfigEntity {
        return ConfigEntity(source.id, source.type, source.value)
    }

    override fun unmap(source: ConfigEntity): ConfigLocal {
        return ConfigLocal(source.id, source.type, source.value)
    }
}
