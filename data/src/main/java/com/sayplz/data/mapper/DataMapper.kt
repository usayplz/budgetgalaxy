package com.sayplz.data.mapper

interface DataMapper<S, R> {
    fun map(source: S): R
    fun unmap(source: R): S
}
