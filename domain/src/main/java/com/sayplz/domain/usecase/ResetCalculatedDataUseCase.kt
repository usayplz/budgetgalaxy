package com.sayplz.domain.usecase

import com.sayplz.domain.entity.CalculatedDataEntity
import com.sayplz.domain.repository.ExpenseRepository

class ResetCalculatedDataUseCase(private val repository: ExpenseRepository) {

    fun execute(): CalculatedDataEntity {
        return repository.resetCalculatedData()
    }
}
