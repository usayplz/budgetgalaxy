package com.sayplz.domain.usecase

import com.sayplz.domain.base.Schedulers
import com.sayplz.domain.entity.TreeEntity
import com.sayplz.domain.repository.ExpenseRepository
import io.reactivex.Single

class GetExpenseCoreUseCase(private val schedulers: Schedulers, private val repository: ExpenseRepository) {

    fun execute(): Single<TreeEntity> {
        return repository.getExpenseCore()
                .subscribeOn(schedulers.io)
                .observeOn(schedulers.ui)
    }
}
