package com.sayplz.domain.usecase

import com.sayplz.domain.base.Schedulers
import com.sayplz.domain.entity.TreeEntity
import com.sayplz.domain.repository.ExpenseRepository
import io.reactivex.Maybe

class GetExpensesByParentIdUseCase(private val schedulers: Schedulers,
                                   private val repository: ExpenseRepository) {

    fun execute(parentId: Long): Maybe<List<TreeEntity>> {
        return repository.getExpensesByParentId(parentId)
                .subscribeOn(schedulers.io)
                .observeOn(schedulers.ui)
    }
}
