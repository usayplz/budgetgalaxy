package com.sayplz.domain.usecase

import com.sayplz.domain.entity.CalculatedDataEntity
import com.sayplz.domain.entity.KeyboardKeyEntity
import com.sayplz.domain.repository.ExpenseRepository

class AddKeyUseCase(private val repository: ExpenseRepository) {

    fun execute(key: KeyboardKeyEntity): CalculatedDataEntity {
        return repository.addKey(key)
    }
}
