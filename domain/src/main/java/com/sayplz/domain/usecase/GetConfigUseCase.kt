package com.sayplz.domain.usecase

import com.sayplz.domain.base.Schedulers
import com.sayplz.domain.entity.ConfigEntity
import com.sayplz.domain.repository.ConfigRepostory
import io.reactivex.Flowable

class GetConfigUseCase(private val schedulers: Schedulers, private val repository: ConfigRepostory) {

    fun execute(): Flowable<List<ConfigEntity>> {
        return repository.getConfig()
                .subscribeOn(schedulers.io)
                .observeOn(schedulers.ui)
    }
}
