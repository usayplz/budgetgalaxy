package com.sayplz.domain.entity

data class ConfigEntity(
        val id: Long = -1,
        val type: ConfigType,
        val value: String
)