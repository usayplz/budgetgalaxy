package com.sayplz.domain.entity

import java.text.DecimalFormatSymbols
import java.util.*

fun getDecimalSeparator() = DecimalFormatSymbols(Locale.getDefault()).decimalSeparator.toString()
fun getThousandSeparator() = DecimalFormatSymbols(Locale.getDefault()).groupingSeparator.toString()

enum class KeyboardKeyEntity(val tag: String) {
    KEY7("7"),
    KEY8("8"),
    KEY9("9"),
    DIVIDE("÷"),

    KEY4("4"),
    KEY5("5"),
    KEY6("6"),
    MULTIPLY("×"),

    KEY1("1"),
    KEY2("2"),
    KEY3("3"),
    MINUS("-"),

    COMMA(getDecimalSeparator()),
    KEY0("0"),
    BACKSPACE("backspace"),
    PLUS("+"),
    LONG_BACKSPACE("long_backspace"),

    OK("ok");
}
