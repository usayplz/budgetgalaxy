package com.sayplz.domain.entity

import java.math.BigDecimal

data class CalculatedDataEntity(
        val value: BigDecimal?,
        val displayValue: String,
        val expression: String
)