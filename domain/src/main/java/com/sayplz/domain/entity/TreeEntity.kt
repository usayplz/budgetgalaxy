package com.sayplz.domain.entity

import java.util.*

/**
 * Created by Sergei Kurikalov
 * on 21.06.18.
 */
data class TreeEntity(
        val id: Long,
        val type: TreeType,
        val parentId: Long,
        val term: String,
        val customTerm: String,
        val fontSize: Int,
        val icon: String,
        val bodyColor: String,
        val iconColor: String,
        val titleColor: String,
        val outerLineColor: String,
        val innerLineColor: String,
        val edgeColor: String,
        val edgeColor2: String,
        val status: TreeStatus,
        val createDate: Date,
        val updateDate: Date
)
