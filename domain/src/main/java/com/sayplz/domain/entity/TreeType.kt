package com.sayplz.domain.entity

enum class TreeType {
    INCOME, EXPENSE, QUESTION, ANSWER
}