package com.sayplz.domain.entity

enum class GenderEntity {
    MALE, FEMALE, UNKNOWN
}
