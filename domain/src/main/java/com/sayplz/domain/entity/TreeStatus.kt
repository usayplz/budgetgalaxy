package com.sayplz.domain.entity

enum class TreeStatus {
    ACTIVE, DISABLE
}
