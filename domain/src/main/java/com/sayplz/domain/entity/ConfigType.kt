package com.sayplz.domain.entity

enum class ConfigType {
    GENDER, BIRTHDAY, CURRENCY
}
