package com.sayplz.domain.base

class MissingUseCaseParameterException(clazz: Class<Any>) :
        IllegalArgumentException("Parameters are mandatory for " + clazz::class.java.simpleName)
