package com.sayplz.domain.base

import io.reactivex.Scheduler

interface Schedulers {
    val io: Scheduler
    val ui: Scheduler
}