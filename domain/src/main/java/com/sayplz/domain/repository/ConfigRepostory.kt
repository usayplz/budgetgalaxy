package com.sayplz.domain.repository

import com.sayplz.domain.entity.ConfigEntity
import com.sayplz.domain.entity.ConfigType
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe

interface ConfigRepostory {
    fun getValue(configType: ConfigType): Maybe<String>

    fun getConfig(): Flowable<List<ConfigEntity>>

    fun insert(config: ConfigEntity): Completable

    fun update(type: ConfigType, value: String): Completable
}
