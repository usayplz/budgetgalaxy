package com.sayplz.domain.repository

import com.sayplz.domain.entity.CalculatedDataEntity
import com.sayplz.domain.entity.TreeEntity
import com.sayplz.domain.entity.KeyboardKeyEntity
import io.reactivex.Maybe
import io.reactivex.Single

interface ExpenseRepository {
    fun addKey(key: KeyboardKeyEntity): CalculatedDataEntity
    fun resetCalculatedData(): CalculatedDataEntity
    fun getExpenseCore(): Single<TreeEntity>
    fun getExpensesByParentId(parentId: Long): Maybe<List<TreeEntity>>
}